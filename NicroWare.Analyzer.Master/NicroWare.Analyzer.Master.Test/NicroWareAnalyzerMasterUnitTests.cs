using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Runtime.CompilerServices;
using TestHelper;

namespace NicroWare.Analyzer.Master.Test
{
    [TestClass]
    public class UnitTest : CodeFixVerifier
    {
        //No diagnostics expected to show up
        [TestMethod]
        public void NoDiagnosticOnEmpty()
        {
            var test = @"";

            VerifyCSharpDiagnostic(test);
        }

        //Diagnostic and CodeFix both triggered and checked for
        [TestMethod]
        public void VarAMissingInRange()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult
            {
                Id = "NicroWareAnalyzerMaster",
                Message = MessageFormatHelper.MissingRestrictions("varA", "InRange"),
                Severity = DiagnosticSeverity.Error,
                Locations =
                    new[] {
                            new DiagnosticResultLocation("Test0.cs", 30, 26)
                        }
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        //Diagnostic and CodeFix both triggered and checked for
        [TestMethod]
        public void VarAHasInRange()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateContains(31, 29, "varA", "InRange")
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void VarAHasInRangeAndNotInRange()
        {
            string test = GetTestCodeForMe();


            var expected = new DiagnosticResult[]
            {
                CreateContains(36, 29, "varA", "InRange"),
                CreateContains(41, 29, "varA", "!InRange"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void ScopePromotion()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateContains(39, 29, "varA", "!InRange", "!InArrayRange"),
                CreateContains(43, 25, "varA"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void OrScopePromotion()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateContains(39, 29, "varA"),
                CreateContains(42, 33, "varA", "InArrayRange"),
                CreateContains(45, 29, "varA", "!InArrayRange"),
                CreateContains(49, 25, "varA", "InRange", "InArrayRange")
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void ObjectFieldRestriction()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateContains(27, 29, "IndexValue", "InRangeOf(values)"),
                CreateContains(32, 29, "IndexValue", "!InRangeOf(values)")
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void MutationInvalidation()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateContains(31, 29, "varA", "InRange"),
                CreateMissing(34, 30, "varA", "InRange"),
                CreateMissing(40, 30, "varA", "InRange"),
                CreateMissing(46, 30, "varA", "InRange")
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void LinkedRestriction()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateContains(32, 29, "a", "InRangeOf(values)"),
                CreateContains(37, 29, "a", "!InRangeOf(values)")
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void LinkedRestrictionDouble()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateContains(33, 29, "a", "InRangeOf(values)", "InRangeOf(values2)"),
                CreateContains(39, 29, "a")
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void LinkedRestrictionDoubleWrongValue()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateContains(34, 29, "a", "InRangeOf(values)"),
                CreateContains(35, 29, "b", "InRangeOf(values2)"),
                CreateMissing(37, 35, "a", "InRangeOf(values2)"),
                CreateContains(41, 29, "a")
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FieldRestriction()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateContains(27, 29, "IndexValue", "InRangeOf(values)"),
                CreateContains(32, 29, "IndexValue", "!InRangeOf(values)")
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FieldRestrictionInvalid()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateMissing(25, 31, "IndexValue", "InRangeOf(values)")
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        private DiagnosticResult CreateMissing(int line, int column, string varName, params string[] restrictions)
        {
            return new DiagnosticResult
            {
                Id = "NicroWareAnalyzerMaster",
                Message = MessageFormatHelper.MissingRestrictions(varName, restrictions),
                Severity = DiagnosticSeverity.Error,
                Locations = new[] { new DiagnosticResultLocation("Test0.cs", line, column) }
            };
        }

        private DiagnosticResult CreateContains(int line, int column, string varName, params string[] restrictions)
        {
            return new DiagnosticResult
            {
                Id = "NicroWareAnalyzerMaster",
                Message = MessageFormatHelper.ContainsRestrictions(varName, restrictions),
                Severity = DiagnosticSeverity.Info,
                Locations = new[] { new DiagnosticResultLocation("Test0.cs", line, column) }
            };
        }

        private string GetTestCodeForMe([CallerMemberName]string name = "")
        {
            return System.IO.File.ReadAllText(System.IO.Path.Combine("TestFiles", name + ".cs")) + Environment.NewLine + System.IO.File.ReadAllText(System.IO.Path.Combine("TestFiles", "Common.cs"));
        }

        private string GetTestCode(string name)
        {
            return System.IO.File.ReadAllText(System.IO.Path.Combine("TestFiles", name + ".cs")) + Environment.NewLine + System.IO.File.ReadAllText(System.IO.Path.Combine("TestFiles", "Common.cs"));
        }

        protected override DiagnosticAnalyzer GetCSharpDiagnosticAnalyzer()
        {
            return new NicroWareAnalyzer();
        }
    }
}
