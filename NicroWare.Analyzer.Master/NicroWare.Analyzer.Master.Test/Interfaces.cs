﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using TestHelper;

namespace NicroWare.Analyzer.Master.Test
{
    [TestClass]
    public class Interfaces : CodeFixVerifier
    {

        [TestMethod]
        public void AnalysisNullCheck()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(30, 13, "sc", "NonNullable"),
                CreateMissing(34, 17, "test", "NonNullable"),
                // CreateMissing(26, 13, "test", "NonNullable"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void AnalysisNullMethodCheck()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(33, 13, "sc", "NonNullable"),
                CreateMissing(33, 13, "",   "NonNullable"),
                CreateMissing(37, 17, "test", "NonNullable"),
                // CreateMissing(26, 13, "test", "NonNullable"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }


        private DiagnosticResult CreateMissing(int line, int column, string varName, params string[] restrictions)
        {
            return new DiagnosticResult
            {
                Id = "NicroWareAnalyzerMaster",
                Message = MessageFormatHelper.MissingRestrictions(varName, restrictions),
                Severity = DiagnosticSeverity.Error,
                Locations = new[] { new DiagnosticResultLocation("Test0.cs", line, column) }
            };
        }

        private DiagnosticResult CreateContains(int line, int column, string varName, params string[] restrictions)
        {
            return new DiagnosticResult
            {
                Id = "NicroWareAnalyzerMaster",
                Message = MessageFormatHelper.ContainsRestrictions(varName, restrictions),
                Severity = DiagnosticSeverity.Info,
                Locations = new[] { new DiagnosticResultLocation("Test0.cs", line, column) }
            };
        }

        private string GetTestCodeForMe([CallerMemberName]string name = "")
        {
            string className = GetType().Name;
            return System.IO.File.ReadAllText(System.IO.Path.Combine("TestFiles", className, name + ".cs")) + Environment.NewLine + System.IO.File.ReadAllText(System.IO.Path.Combine("TestFiles", "Common.cs"));
        }

        protected override DiagnosticAnalyzer GetCSharpDiagnosticAnalyzer()
        {
            return new NicroWareAnalyzer();
        }
    }
}
