﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using TestHelper;

namespace NicroWare.Analyzer.Master.Test
{
    [TestClass]
    public class Nullable : CodeFixVerifier
    {
        [TestMethod]
        public void NullableField()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(13, 13, "s", "NonNullable"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void CheckNullableField()
        {
            string test = GetTestCodeForMe();
            VerifyCSharpDiagnostic(test);
        }

        [TestMethod]
        public void NullableMethod()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(24, 13, "s", "NonNullable"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void CheckNullableMethod()
        {
            string test = GetTestCodeForMe();
            VerifyCSharpDiagnostic(test);
        }

        [TestMethod]
        public void NullableArgument()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(22, 13, "s", "NonNullable"),
                CreateMissing(27, 13, "s", "NonNullable"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void NullableArgumentCheck()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(28, 25, "s", "NonNullable"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void UnkownNullableArgumentCheck()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(28, 25, "s", "NonNullable"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void NullableConstructor()
        {
            string test = GetTestCodeForMe();
            VerifyCSharpDiagnostic(test);
        }

        [TestMethod]
        public void NullableConstructorCheck()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(41, 47, "s", "NonNullable"),
                CreateMissing(42, 62, "s", "NonNullable"),
                CreateMissing(43, 28, "s", "NonNullable"),
                CreateMissing(44, 43, "s", "NonNullable"),
                CreateMissing(45, 40, "s", "NonNullable"),
                CreateMissing(46, 55, "s", "NonNullable"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void NullableFieldAccess()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateMissing(24, 13, "sc", "NonNullable"),
                CreateMissing(25, 27, "sc", "NonNullable"),
                // CreateMissing(26, 13, "test", "NonNullable"),
            };
            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void NullablePropAccess()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(24, 13, "sc", "NonNullable"),
                CreateMissing(25, 27, "sc", "NonNullable"),
                // CreateMissing(26, 13, "test", "NonNullable"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void NullableFieldAccess2()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateMissing(30, 13, "sc", "NonNullable"),
                CreateMissing(34, 17, "test", "NonNullable"),
                // CreateMissing(26, 13, "test", "NonNullable"),
            };
            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void NullablePropAccess2()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(30, 13, "sc", "NonNullable"),
                CreateMissing(34, 17, "test", "NonNullable"),
                // CreateMissing(26, 13, "test", "NonNullable"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void NullableFieldAccess3()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateMissing(22, 13, "sc.s", "NonNullable"),
                CreateMissing(24, 13, "test", "NonNullable"),
                // CreateMissing(26, 13, "test", "NonNullable"),
            };
            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void NullableInObjectCreation()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateMissing(41, 43, "s", "NonNullable"),
                CreateMissing(42, 43, "s", "NonNullable"),
                // CreateMissing(26, 13, "test", "NonNullable"),
            };
            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void NullableInObjectCreationCheck()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                // CreateMissing(26, 13, "test", "NonNullable"),
            };
            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void NullableNullChecking()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                // CreateMissing(26, 13, "test", "NonNullable"),
            };
            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void NullableInIf()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateMissing(30, 17, "sc", "NonNullable"),
                CreateMissing(32, 31, "sc", "NonNullable"),
            };
            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void NullableInIfCheck()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                // CreateMissing(26, 13, "test", "NonNullable"),
            };
            VerifyCSharpDiagnostic(test, expected);
        }


        [TestMethod]
        public void LocalNullAssignment()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateMissing(22, 13, "local", "NonNullable"),
                CreateMissing(26, 13, "local2", "NonNullable"),
            };
            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void CheckNullableReturnMethod()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateMissing(22, 20, "", "NonNullable"),
                CreateMissing(27, 20, "", "NonNullable"),
            };
            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void ConditionalNull()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateMissing(20, 13, "ss", "NonNullable"),
                CreateMissing(22, 24, "ss", "NonNullable"),
                CreateMissing(24, 13, "b", "NonNullable"),
                /*CreateMissing(23, 20, "S", "NonNullable"),
                CreateMissing(25, 20, "SS", "NonNullable"),
                CreateMissing(26, 20, "S", "NonNullable"),*/
            };
            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void NonNullableFieldAssignment()
        {
            string test = GetTestCodeForMe();
            var expected = new DiagnosticResult[]
            {
                CreateMissing(8, 27, "", "NonNullable"),
                CreateMissing(22, 21, "", "NonNullable"),
            };
            VerifyCSharpDiagnostic(test, expected);
        }

        private DiagnosticResult CreateMissing(int line, int column, string varName, params string[] restrictions)
        {
            return new DiagnosticResult
            {
                Id = "NicroWareAnalyzerMaster",
                Message = MessageFormatHelper.MissingRestrictions(varName, restrictions),
                Severity = DiagnosticSeverity.Error,
                Locations = new[] { new DiagnosticResultLocation("Test0.cs", line, column) }
            };
        }

        private DiagnosticResult CreateContains(int line, int column, string varName, params string[] restrictions)
        {
            return new DiagnosticResult
            {
                Id = "NicroWareAnalyzerMaster",
                Message = MessageFormatHelper.ContainsRestrictions(varName, restrictions),
                Severity = DiagnosticSeverity.Info,
                Locations = new[] { new DiagnosticResultLocation("Test0.cs", line, column) }
            };
        }

        private string GetTestCodeForMe([CallerMemberName]string name = "")
        {
            string className = GetType().Name;
            return System.IO.File.ReadAllText(System.IO.Path.Combine("TestFiles", className, name + ".cs")) + Environment.NewLine + System.IO.File.ReadAllText(System.IO.Path.Combine("TestFiles", "Common.cs"));
        }

        protected override DiagnosticAnalyzer GetCSharpDiagnosticAnalyzer()
        {
            return new NicroWareAnalyzer();
        }
    }
}
