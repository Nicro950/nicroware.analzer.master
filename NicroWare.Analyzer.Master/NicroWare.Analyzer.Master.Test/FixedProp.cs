﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using TestHelper;

namespace NicroWare.Analyzer.Master.Test
{
    [TestClass]
    public class FixedProp : CodeFixVerifier
    {
        [TestMethod]
        public void FixedPropRestriction()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateContains(29, 29, "someAge", "Age"),
                CreateContains(30, 29, "Age", "Age"),
                CreateContains(32, 29, "Age", "Age"),
                CreateContains(37, 29, "someAge", "!Age"),
                CreateContains(38, 29, "Age", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedPropRestrictionInvalid()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(27, 19, "someAge", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedPropObjectRestriction()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateContains(31, 29, "someAge", "Age"),
                CreateContains(32, 29, "p.Age", "Age"),
                CreateContains(34, 29, "p.Age", "Age"),
                CreateContains(39, 29, "someAge", "!Age"),
                CreateContains(40, 29, "p.Age", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedPropObjectRestrictionInvalid()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(29, 21, "someAge", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedPropObjectInitFixedRestriction()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                // CreateMissing(29, 23, "", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedPropObjectInitRestriction()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(29, 23, "", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }


        [TestMethod]
        public void FixedPropObjectInitFromParameter()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                // CreateMissing(29, 23, "", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedPropObjectInitReturnFromParameter()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                // CreateMissing(29, 23, "", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedPropObjectInitReturnFromParameterInvalid()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(29, 39, "p.Age2", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedPropMultiObjectInvalid()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(35, 37, "topic", "HasValue"),
                CreateMissing(36, 39, "content", "HasValue"),
                CreateMissing(37, 40, "authorId", "Id"),
                CreateMissing(38, 42, "authorName", "HasValue"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        private DiagnosticResult CreateMissing(int line, int column, string varName, params string[] restrictions)
        {
            return new DiagnosticResult
            {
                Id = "NicroWareAnalyzerMaster",
                Message = MessageFormatHelper.MissingRestrictions(varName, restrictions),
                Severity = DiagnosticSeverity.Error,
                Locations = new[] { new DiagnosticResultLocation("Test0.cs", line, column) }
            };
        }

        private DiagnosticResult CreateContains(int line, int column, string varName, params string[] restrictions)
        {
            return new DiagnosticResult
            {
                Id = "NicroWareAnalyzerMaster",
                Message = MessageFormatHelper.ContainsRestrictions(varName, restrictions),
                Severity = DiagnosticSeverity.Info,
                Locations = new[] { new DiagnosticResultLocation("Test0.cs", line, column) }
            };
        }

        private string GetTestCodeForMe([CallerMemberName]string name = "")
        {
            string className = GetType().Name;
            return System.IO.File.ReadAllText(System.IO.Path.Combine("TestFiles", className, name + ".cs")) + Environment.NewLine + System.IO.File.ReadAllText(System.IO.Path.Combine("TestFiles", "Common.cs"));
        }

        protected override DiagnosticAnalyzer GetCSharpDiagnosticAnalyzer()
        {
            return new NicroWareAnalyzer();
        }
    }
}
