﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using TestHelper;

namespace NicroWare.Analyzer.Master.Test
{
    [TestClass]
    public class FixedField : CodeFixVerifier
    {
        [TestMethod]
        public void FixedFieldRestriction()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateContains(29, 29, "someAge", "Age"),
                CreateContains(30, 29, "Age", "Age"),
                CreateContains(32, 29, "Age", "Age"),
                CreateContains(37, 29, "someAge", "!Age"),
                CreateContains(38, 29, "Age", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedFieldRestrictionInvalid()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(27, 19, "someAge", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedFieldObjectRestriction()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateContains(31, 29, "someAge", "Age"),
                CreateContains(32, 29, "p.Age", "Age"),
                CreateContains(34, 29, "p.Age", "Age"),
                CreateContains(39, 29, "someAge", "!Age"),
                CreateContains(40, 29, "p.Age", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedFieldObjectRestrictionInvalid()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(29, 21, "someAge", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedFieldObjectInitFixedRestriction()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                // CreateMissing(29, 23, "", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedFieldObjectInitRestriction()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(29, 23, "", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        //FixedFieldObjectInitFromParameter
        [TestMethod]
        public void FixedFieldObjectInitFromParameter()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedFieldObjectInitReturnFromParameter()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedFieldObjectInitReturnFromParameterInvalid()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(29, 39, "p.Age2", "Age"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void FixedFieldMultiObjectInvalid()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(35, 37, "topic", "HasValue"),
                CreateMissing(36, 39, "content", "HasValue"),
                CreateMissing(37, 40, "authorId", "Id"),
                CreateMissing(38, 42, "authorName", "HasValue"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        private DiagnosticResult CreateMissing(int line, int column, string varName, params string[] restrictions)
        {
            return new DiagnosticResult
            {
                Id = "NicroWareAnalyzerMaster",
                Message = MessageFormatHelper.MissingRestrictions(varName, restrictions),
                Severity = DiagnosticSeverity.Error,
                Locations = new[] { new DiagnosticResultLocation("Test0.cs", line, column) }
            };
        }

        private DiagnosticResult CreateContains(int line, int column, string varName, params string[] restrictions)
        {
            return new DiagnosticResult
            {
                Id = "NicroWareAnalyzerMaster",
                Message = MessageFormatHelper.ContainsRestrictions(varName, restrictions),
                Severity = DiagnosticSeverity.Info,
                Locations = new[] { new DiagnosticResultLocation("Test0.cs", line, column) }
            };
        }

        private string GetTestCodeForMe([CallerMemberName]string name = "")
        {
            string className = GetType().Name;
            return System.IO.File.ReadAllText(System.IO.Path.Combine("TestFiles", className, name + ".cs")) + Environment.NewLine + System.IO.File.ReadAllText(System.IO.Path.Combine("TestFiles", "Common.cs"));
        }

        protected override DiagnosticAnalyzer GetCSharpDiagnosticAnalyzer()
        {
            return new NicroWareAnalyzer();
        }
    }
}
