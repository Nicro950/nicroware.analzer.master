﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class Person
    {
        [SubType("Age")]
        public int Age { get; set; } = 2;
    }

    class FieldRestriction
    {
        [CheckMethod("Age")]
        public static bool IsAge(int index)
        {
            return index >= 0 && index < 120;
        }

        public static void PrintAge([SubType("Age")]int age)
        {
            Console.WriteLine(age);
        }

        public static void SomeMain()
        {
            Person p = new Person();
            Person p2 = new Person();
            int someAge = 4;
            if (IsAge(someAge))
            {
                p.Age = someAge;
                p2.Age = p.Age;
                PrintAge(p2.Age);
                Person p3 = new Person
                {
                    Age = p2.Age
                };
            }
            else
            {

            }
        }
    }
}
