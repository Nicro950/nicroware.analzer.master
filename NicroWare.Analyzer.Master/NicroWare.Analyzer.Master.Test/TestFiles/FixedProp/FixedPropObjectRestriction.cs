﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class Person
    {
        [SubType("Age")]
        public int Age { get; set; } = 2;
    }

    class FieldRestriction
    {
        [CheckMethod("Age")]
        public static bool IsAge(int index)
        {
            return index >= 0 && index < 120;
        }

        public static void PrintAge([SubType("Age")]int age)
        {
            Console.WriteLine(age);
        }

        public static void SomeMain()
        {
            Person p = new Person();
            int someAge = 4;
            if (IsAge(someAge))
            {
                Scope.Check(someAge);
                Scope.Check(p.Age);
                p.Age = someAge;
                Scope.Check(p.Age);
                PrintAge(p.Age);
            }
            else
            {
                Scope.Check(someAge);
                Scope.Check(p.Age);
            }
        }
    }
}
