﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using NicroWare.Analyzer.ExtLib;

namespace ConsoleApplication1
{
    class TypeName
    {
        public static void NeedsInRange([SubType("InRange")]int someParameter)
        {
            NeedsInRange(someParameter);
        }

        [CheckMethod("InRange")]
        public static bool IsInRange(int a)
        {
            if (a >= 0 && a < 10)
                return true;
            return false;
        }

        static int[] GlobalArray = new int[] { 0, 1, 2, 3, 4, 5, 6 };

        [CheckMethod("InArrayRange")]
        public static bool IsInRangeOfArray(int index)
        {
            return index >= 0 && index < GlobalArray.Length;
        }

        public static void SomeMain()
        {
            int varA = 5;
            if (!IsInRange(varA) || !IsInRangeOfArray(varA)) // Else scope promotion
            {
                Scope.Check(varA);
                if (IsInRangeOfArray(varA))
                {
                    Scope.Check(varA);
                    return;
                }
                Scope.Check(varA);
                return;
            }

            Scope.Check(varA);
        }
    }
}