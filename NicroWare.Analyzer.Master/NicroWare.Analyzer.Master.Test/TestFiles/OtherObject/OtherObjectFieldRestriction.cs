﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class OtherObject
    {
        public int IndexValue;
    }

    class FieldRestriction
    {
        public int[] values = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        [CheckMethod("InRangeOf")]
        public bool IsInRangeOf(int index, int[] range)
        {
            return index >= 0 && index < range.Length;
        }

        public int GetValueFromArray([SubType("InRangeOf")]int index, [SubType("InRangeOf(this)")] int[] values)
        {
            return values[index];
        }

        public void SomeMain()
        {
            int a = 3;
            OtherObject oo = new OtherObject();

            if (IsInRangeOf(oo.IndexValue, values))
            {
                Scope.Check(oo.IndexValue);
                GetValueFromArray(oo.IndexValue, values);
            }
            else
            {
                Scope.Check(oo.IndexValue);
            }
        }
    }
}
