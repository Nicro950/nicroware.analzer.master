﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class OtherObject
    {
        public int IndexValue;
        public int[] Values = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    }

    class FieldRestriction
    {
        [CheckMethod("InRangeOf")]
        public bool IsInRangeOf(int index, int[] range)
        {
            return index >= 0 && index < range.Length;
        }

        public int GetValueFromArray([SubType("InRangeOf")]int index, [SubType("InRangeOf(this)")] int[] values)
        {
            return values[index];
        }

        public void SomeMain()
        {
            OtherObject oo = new OtherObject();

            if (IsInRangeOf(oo.IndexValue, oo.Values))
            {
                Scope.Check(oo.IndexValue);
                GetValueFromArray(oo.IndexValue, oo.Values);
            }
            else
            {
                Scope.Check(oo.IndexValue);
            }
        }
    }
}
