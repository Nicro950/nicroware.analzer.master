﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class FieldRestriction
    {
        [SubType("Age")]
        public static int Age = 2;

        public static int[] values = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        [CheckMethod("Age")]
        public static bool IsAge(int index)
        {
            return index >= 0 && index < 120;
        }

        public static void PrintAge([SubType("Age")]int age)
        {
            Console.WriteLine(age);
        }

        public static void SomeMain()
        {
            int someAge = 4;
            if (IsAge(someAge))
            {
                Scope.Check(someAge);
                Scope.Check(Age);
                Age = someAge;
                Scope.Check(Age);
                PrintAge(Age);
            }
            else
            {
                Scope.Check(someAge);
                Scope.Check(Age);
            }
        }
    }
}
