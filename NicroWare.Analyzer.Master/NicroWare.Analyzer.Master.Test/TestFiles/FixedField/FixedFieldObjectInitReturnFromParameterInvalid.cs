﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class Person
    {
        [SubType("Age")]
        public int Age = 2;

        public int Age2 = 2;
    }

    class FieldRestriction
    {
        [CheckMethod("Age")]
        public static bool IsAge(int index)
        {
            return index >= 0 && index < 120;
        }

        public static void PrintAge([SubType("Age")]int age)
        {
            Console.WriteLine(age);
        }

        public static Person InitObject(Person p)
        {
            return new Person { Age = p.Age2 };
        }

        public static void SomeMain()
        {
            Person p = new Person();
            int someAge = 4;
            if (IsAge(someAge))
            {
                p.Age = someAge;
                InitObject(p);
            }
            else
            {

            }
        }
    }
}
