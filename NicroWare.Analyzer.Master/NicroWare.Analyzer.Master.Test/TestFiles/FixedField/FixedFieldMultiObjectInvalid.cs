﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class Article
    {
        [SubType("HasValue")]
        public string Topic;
        [SubType("HasValue")]
        public string Content;
        [SubType("Id")]
        public int AuthorId;
        [SubType("HasValue")]
        public string AuthorName;
    }

    class FieldRestriction
    {
        [CheckMethod("HasValue")]
        public static bool IsHasValue(string value)
        {
            return !string.IsNullOrWhiteSpace(value);
        }

        [CheckMethod("Id")]
        public static bool IsId(int index)
        {
            return index > 0;
        }

        public async Article Update(string topic, string content, Guid authorId, string authorName)
        {
            Article existingArticle = new Article();
            existingArticle.Topic = topic;
            existingArticle.Content = content;
            existingArticle.AuthorId = authorId;
            existingArticle.AuthorName = authorName;

            return existingArticle;
        }

        public static void SomeMain()
        {
            Update("topic", "content", Guid.Empty, "authorName");
        }
    }
}
