﻿using System;
using System.Threading.Tasks;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles.TaskAwait
{
    class AwaitUnpack
    {
        [Nullable]
        public static Task<string> GetSomeTask()
        {
            return Task.FromResult("Hello World");
        }

        public static async Task SomeMain()
        {
            string result = await GetSomeTask();
            result.Trim();
            string result2 = await GetSomeTask().ConfigureAwait(false);
            result2.Trim();
        }
    }
}
