﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class OtherClass
    {
        public OtherClass([Nullable]string test)
        {

        }

        public OtherClass(OtherClass otherClass)
        {

        }
    }

    class FieldRestriction
    {
        [CheckNotNull]
        public static bool IsNotNull(object s)
        {
            return s != null;
        }

        [Nullable]
        public static string ReturnsNull()
        {
            return null;
        }

        public static void DoSomething(OtherClass s)
        {

        }

        public static void SomeMain()
        {
            string s = ReturnsNull();
            OtherClass first = new OtherClass(s);
            OtherClass first = new OtherClass(new OtherClass(s));
            new OtherClass(s);
            new OtherClass(new OtherClass(s));
            DoSomething(new OtherClass(s));
            DoSomething(new OtherClass(new OtherClass(s)));
        }
    }
}
