﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class FieldRestriction
    {
        [CheckNotNull]
        public static bool IsNotNull(object s)
        {
            return s != null;
        }

        [Nullable]
        public static string ReturnsNull()
        {
            return null;
        }

        public static void DoSomething(string s)
        {

        }

        public static void SomeMain()
        {
            string s = ReturnsNull();
            string.Copy(s);
        }
    }
}
