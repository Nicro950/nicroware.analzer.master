﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class SomeClass
    {
        [Nullable]
        public string s = null;
    }

    class FieldRestriction
    {
        public SomeClass GetNullClass()
        {
            return new SomeClass();
        }

        public static void SomeMain()
        {
            SomeClass sc = GetNullClass();
            sc.s.Trim();
            string test = sc.s;
            test.Trim();
            sc.s = null;
            test = sc.s;
        }
    }
}
