﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class SomeClass
    {
        [Nullable]
        public string s = null;
    }

    class FieldRestriction
    {

        [Nullable]
        public SomeClass GetNullClass()
        {
            return null;
        }

        [CheckNotNull]
        public bool CheckNotNull([Nullable]object o)
        {
            return o != null;
        }

        public static void SomeMain()
        {
            SomeClass sc = GetNullClass();
            sc.s.Trim();
            if (CheckNotNull(sc))
            {
                string test = sc.s;
                test.Trim();
            }
        }
    }
}
