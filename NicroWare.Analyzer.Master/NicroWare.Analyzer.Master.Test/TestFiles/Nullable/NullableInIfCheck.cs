﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class SomeClass
    {
        [Nullable]
        public string s { get; set; }
    }

    class FieldRestriction
    {

        [Nullable]
        public SomeClass GetNullClass()
        {
            return null;
        }

        [CheckNotNull]
        public bool CheckNotNull(object o)
        {
            return o != null;
        }

        public static void SomeMain()
        {
            SomeClass sc = GetNullClass();
            if (sc != null && sc.s != null && sc.s != "")
            {
                string test = sc.s;
                test.Trim();
            }
        }
    }
}
