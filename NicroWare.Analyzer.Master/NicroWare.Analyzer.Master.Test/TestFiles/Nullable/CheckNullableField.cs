﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class FieldRestriction
    {
        [Nullable]
        static string s = null;

        [CheckNotNull]
        public static bool IsNotNull([Nullable]object s)
        {
            return s != null;
        }

        public static void SomeMain()
        {
            if (IsNotNull(s))
            {
                s.Trim();
            }
        }
    }
}
