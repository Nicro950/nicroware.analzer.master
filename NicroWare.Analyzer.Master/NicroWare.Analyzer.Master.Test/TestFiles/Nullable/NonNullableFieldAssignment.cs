﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class SomeClass
    {
        public string s = null; // Should this be same as nullable or non nullable?
        public string s2 = "";
    }

    class FieldRestriction
    {
        public SomeClass GetNullClass()
        {
            return new SomeClass();
        }

        public static void SomeMain()
        {
            SomeClass sc = GetNullClass();
            sc.s2 = null;
        }
    }
}
