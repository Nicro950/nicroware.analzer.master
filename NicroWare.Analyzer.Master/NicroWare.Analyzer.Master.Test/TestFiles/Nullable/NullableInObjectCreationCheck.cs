﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class OtherClass
    {
        public OtherClass(string test)
        {

        }

        public OtherClass(OtherClass otherClass)
        {

        }
    }

    class FieldRestriction
    {
        [CheckNotNull]
        public static bool IsNotNull([Nullable]object s)
        {
            return s != null;
        }

        [Nullable]
        public static string ReturnsNull()
        {
            return null;
        }

        public static void DoSomething(object o)
        {

        }

        public static void SomeMain()
        {
            string s = ReturnsNull();
            if (IsNotNull(s))
            {
                DoSomething(new { Something = s.Trim() });
                DoSomething(new { Something = s.Length.ToString });
            }

            if (!IsNotNull(s))
            {
                return;
            }

            DoSomething(new { Something = s.Trim() });
            DoSomething(new { Something = s.Length.ToString });
        }
    }
}
