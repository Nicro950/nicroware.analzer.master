﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class FieldRestriction
    {
        [Nullable]
        static string s = null;

        public static void SomeMain()
        {
            if (!(s == null))
            {
                s.Trim();
            }
            if (s != null)
            {
                s.Trim();
            }
            if (null != s)
            {
                s.Trim();
            }
            if (!(null == s))
            {
                s.Trim();
            }
        }
    }
}
