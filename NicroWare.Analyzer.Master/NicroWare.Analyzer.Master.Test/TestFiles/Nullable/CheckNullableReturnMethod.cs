﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class FieldRestriction
    {
        [CheckNotNull]
        public static bool IsNotNull([Nullable]object s)
        {
            return s != null;
        }

        [Nullable]
        public static string ReturnsNull()
        {
            return null;
        }

        public static string TriesReturnNull()
        {
            return null;
        }

        public static string TriesReturnSomethingThatCanBeNull()
        {
            return ReturnsNull();
        }


        public static void SomeMain()
        {

        }
    }
}
