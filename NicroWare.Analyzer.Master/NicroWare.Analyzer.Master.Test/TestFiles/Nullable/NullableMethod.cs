﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class FieldRestriction
    {
        [CheckNotNull]
        public static bool IsNotNull(object s)
        {
            return s != null;
        }

        [Nullable]
        public static string ReturnsNull()
        {
            return null;
        }


        public static void SomeMain()
        {
            string s = ReturnsNull();
            s.Trim();
        }
    }
}
