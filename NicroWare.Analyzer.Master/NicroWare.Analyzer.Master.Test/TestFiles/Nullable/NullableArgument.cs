﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class FieldRestriction
    {
        [CheckNotNull]
        public static bool IsNotNull(object s)
        {
            return s != null;
        }

        [Nullable]
        public static string ReturnsNull()
        {
            return null;
        }

        public static void DoSomething([Nullable]string s)
        {
            s.Trim();
        }

        public static void DoSomething2(string s = null)
        {
            s.Trim();
        }

        public static void DoSomething3(int? a)
        {
            a.ToString();
        }

        public static void SomeMain()
        {
            string s = ReturnsNull();
            DoSomething(s);
            DoSomething2(s);
            DoSomething3(null);
        }
    }
}
