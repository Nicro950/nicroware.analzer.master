﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NicroWare.Analyzer.Master.Test.TestFiles.Nullable
{
    class LocalNullAssignment
    {
        public static void SomeMain()
        {
            string local = "";

            local.Trim();

            if (local != null)
            {
                local.Trim();
            }

            local = null;

            local.Trim();

            string local2 = null;

            local2.Trim();

            if (local2 != null)
            {
                local2.Trim();
            }
        }
    }
}
