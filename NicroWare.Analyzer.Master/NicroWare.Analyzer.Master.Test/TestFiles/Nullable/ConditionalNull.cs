﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class SomeString
    {
        [Nullable]
        public string S { get; set; } = null;

        [Nullable]
        public SomeString SS { get; set; } = null;
    }
    class FieldRestriction
    {
        public static void SomeMain()
        {
            SomeString ss = null;

            ss.SS.SS.SS.SS.S.Trim();

            string a = ss.S.Trim();
            string b = ss?.S?.Trim();
            b.Trim();
            /*string c = ss?.S?.Trim();
            string d = ss?.SS.S.Trim();
            string e = ss?.SS?.S.Trim();
            string e = ss?.SS?.S?.Trim();
            string v = ss?.SS?.SS?.SS?.SS?.S?.Trim();*/
        }
    }
}
