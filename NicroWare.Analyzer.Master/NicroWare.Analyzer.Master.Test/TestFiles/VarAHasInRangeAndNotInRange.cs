﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using NicroWare.Analyzer.ExtLib;

namespace ConsoleApplication1
{
    class TypeName
    {
        public static void NeedsInRange([SubType("InRange")]int someParameter)
        {
            NeedsInRange(someParameter);
        }

        public static void NeedsNotInRange([SubType("!InRange")]int someParameter)
        {
            NeedsNotInRange(someParameter);
        }

        [CheckMethod("InRange")]
        public static bool IsInRange(int a)
        {
            if (a >= 0 && a < 10)
                return true;
            return false;
        }

        public static void SomeMain()
        {
            int varA = 5;
            if (IsInRange(varA))
            {
                Scope.Check(varA);
                NeedsInRange(varA); // Compile error, not checked (Should work in future)
            }
            else
            {
                Scope.Check(varA);
                NeedsNotInRange(varA);
            }
        }
    }
}