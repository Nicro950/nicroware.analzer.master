﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using NicroWare.Analyzer.ExtLib;

namespace ConsoleApplication1
{
    class TypeName
    {
        // Assositated sub types
        [CheckMethod("InRangeOf")]
        public static bool IsInRangeOf(int index, int[] range)
        {
            return index >= 0 && index < range.Length;
        }

        public static int GetValueFromArray([SubType("InRangeOf")]int index, [SubType("InRangeOf(this)")]int[] range)
        {
            return range[index];
        }

        public static void SomeMain()
        {
            int[] values = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int[] values2 = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12 };
            int a = 5;

            if (IsInRangeOf(a, values) && IsInRangeOf(a, values2))
            {
                Scope.Check(a);
                GetValueFromArray(a, values); // Linked argument compiles file
                GetValueFromArray(a, values2); // Linked to wrong parameter, wont compile
            }
            else
            {
                Scope.Check(a);
            }
        }
    }
}