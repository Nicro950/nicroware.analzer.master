﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    interface ISomeClass
    {
        [Nullable]
        public string s { get; set; }

        [Nullable]
        public string GetSomething();
    }

    class FieldRestriction
    {

        [Nullable]
        public ISomeClass GetNullClass()
        {
            return null;
        }

        [CheckNotNull]
        public bool CheckNotNull([Nullable]object o)
        {
            return o != null;
        }

        public static void SomeMain()
        {
            ISomeClass sc = GetNullClass();
            sc.GetSomething().Trim();
            if (CheckNotNull(sc))
            {
                string test = sc.GetSomething();
                test.Trim();
            }
        }
    }
}
