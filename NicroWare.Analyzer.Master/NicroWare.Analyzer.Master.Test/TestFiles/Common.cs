﻿using System;
namespace NicroWare.Analyzer.ExtLib
{
    public class CheckMethodAttribute : Attribute
    {
        public string Name { get; set; }

        public CheckMethodAttribute(string name)
        {
            this.Name = name;
        }
    }

    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class SubTypeAttribute : Attribute
    {
        public string Name { get; set; }

        public SubTypeAttribute(string name)
        {
            this.Name = name;
        }
    }

    [AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
    public class OptTypeAttribute : Attribute
    {
        public string Name { get; set; }

        public OptTypeAttribute(string name)
        {
            this.Name = name;
        }
    }

    public class NullableAttribute : Attribute
    {

    }

    public class CheckNotNullAttribute : Attribute
    {

    }

    public class Scope
    {
        public static void Check(object o)
        {

        }
    }
}
