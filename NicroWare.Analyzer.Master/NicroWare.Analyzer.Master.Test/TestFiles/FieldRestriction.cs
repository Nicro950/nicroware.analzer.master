﻿using System;
using NicroWare.Analyzer.ExtLib;

namespace NicroWare.Analyzer.Master.Test.TestFiles
{
    class FieldRestriction
    {
        public static int IndexValue;

        public static int[] values = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        [CheckMethod("InRangeOf")]
        public static bool IsInRangeOf(int index, int[] range)
        {
            return index >= 0 && index < range.Length;
        }

        public static int GetValueFromArray([SubType("InRangeOf")]int index, [SubType("InRangeOf(this)")] int[] values)
        {
            return values[index];
        }

        public static void SomeMain()
        {
            if (IsInRangeOf(IndexValue, values))
            {
                Scope.Check(IndexValue);
                GetValueFromArray(IndexValue, values);
            }
            else
            {
                Scope.Check(IndexValue);
            }
        }
    }
}
