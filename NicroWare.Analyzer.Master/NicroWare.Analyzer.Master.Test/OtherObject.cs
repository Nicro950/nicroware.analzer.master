﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using TestHelper;

namespace NicroWare.Analyzer.Master.Test
{
    [TestClass]
    public class OtherObject : CodeFixVerifier
    {
        [TestMethod]
        public void OtherObjectFieldRestriction()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateContains(33, 29, "oo.IndexValue", "InRangeOf(values)"),
                CreateContains(38, 29, "oo.IndexValue", "!InRangeOf(values)")
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void OtherObjectRecursiveFieldRestriction()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateContains(34, 29, "oo.OtherObject.IndexValue", "InRangeOf(values)"),
                CreateContains(39, 29, "oo.OtherObject.IndexValue", "!InRangeOf(values)")
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void OtherObjectFieldError()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(30, 31, "oo.IndexValue", "InRangeOf(values)"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void OtherObjectIndexFieldRestriction()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateContains(31, 29, "oo.IndexValue", "InRangeOf(oo.Values)"),
                CreateContains(36, 29, "oo.IndexValue", "!InRangeOf(oo.Values)"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        [TestMethod]
        public void OtherObjectIndexError()
        {
            string test = GetTestCodeForMe();

            var expected = new DiagnosticResult[]
            {
                CreateMissing(29, 31, "oo.IndexValue", "InRangeOf(oo.Values)"),
            };

            VerifyCSharpDiagnostic(test, expected);
        }

        private DiagnosticResult CreateMissing(int line, int column, string varName, params string[] restrictions)
        {
            return new DiagnosticResult
            {
                Id = "NicroWareAnalyzerMaster",
                Message = MessageFormatHelper.MissingRestrictions(varName, restrictions),
                Severity = DiagnosticSeverity.Error,
                Locations = new[] { new DiagnosticResultLocation("Test0.cs", line, column) }
            };
        }

        private DiagnosticResult CreateContains(int line, int column, string varName, params string[] restrictions)
        {
            return new DiagnosticResult
            {
                Id = "NicroWareAnalyzerMaster",
                Message = MessageFormatHelper.ContainsRestrictions(varName, restrictions),
                Severity = DiagnosticSeverity.Info,
                Locations = new[] { new DiagnosticResultLocation("Test0.cs", line, column) }
            };
        }

        private string GetTestCodeForMe([CallerMemberName]string name = "")
        {
            string className = GetType().Name;
            return System.IO.File.ReadAllText(System.IO.Path.Combine("TestFiles", className, name + ".cs")) + Environment.NewLine + System.IO.File.ReadAllText(System.IO.Path.Combine("TestFiles", "Common.cs"));
        }

        protected override DiagnosticAnalyzer GetCSharpDiagnosticAnalyzer()
        {
            return new NicroWareAnalyzer();
        }
    }
}
