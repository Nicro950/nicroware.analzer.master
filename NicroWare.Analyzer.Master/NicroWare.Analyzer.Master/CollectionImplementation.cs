﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;

namespace NicroWare.Analyzer.Master
{
    public interface IClassInfoStore
    {
        bool ContainsKey(string key);
        ClassInfo this[string key] { get; }
    }

    public interface ITypeStore
    {

    }

    public interface IMethodStore
    {
        bool ContainsCheckMethod(string methodName);
        IReadOnlyCollection<SubTypeCheckMethod> GetChecksFor(string methodName);

        bool ContainsMethodInfo(string methodName);
        ArugmentListTypeInfo GetMethodInfo(string methodName);
        bool ContainsNullCheckMethod(string methodName);
        bool ContainsNullableMethod(string methodName);
        bool ContainsPassthroughMethod(string methodName);
    }

    public class CollectionMethodStore : IMethodStore
    {
        private readonly FileInfoCollection collection;

        public CollectionMethodStore(FileInfoCollection collection)
        {
            this.collection = collection;
        }

        public bool ContainsCheckMethod(string methodName)
        {
            foreach (var file in collection.Files)
            {
                if (file.Value.MethodStore.ContainsCheckMethod(methodName))
                {
                    return true;
                }
            }
            return false;
        }

        public IReadOnlyCollection<SubTypeCheckMethod> GetChecksFor(string methodName)
        {
            foreach (var file in collection.Files)
            {
                if (file.Value.MethodStore.ContainsCheckMethod(methodName))
                {
                    return file.Value.MethodStore.GetChecksFor(methodName);
                }
            }
            throw new KeyNotFoundException();
        }

        public bool ContainsMethodInfo(string methodName)
        {
            foreach (var file in collection.Files)
            {
                if (file.Value.MethodStore.ContainsMethodInfo(methodName))
                {
                    return true;
                }
            }
            return false;
        }

        public ArugmentListTypeInfo GetMethodInfo(string methodName)
        {
            foreach (var file in collection.Files)
            {
                if (file.Value.MethodStore.ContainsMethodInfo(methodName))
                {
                    return file.Value.MethodStore.GetMethodInfo(methodName);
                }
            }
            throw new KeyNotFoundException();
        }

        public bool ContainsNullCheckMethod(string methodName)
        {
            return collection.Files.Values.FirstOrDefault(x => x.MethodStore.ContainsNullCheckMethod(methodName)) != null;
        }

        public bool ContainsNullableMethod(string methodName)
        {
            return collection.Files.Values.FirstOrDefault(x => x.MethodStore.ContainsNullableMethod(methodName)) != null;
        }

        public bool ContainsPassthroughMethod(string methodName)
        {
            return collection.Files.Values.FirstOrDefault(x => x.MethodStore.ContainsPassthroughMethod(methodName)) != null;
        }
    }

    public class CollectionTypeStore : ITypeStore
    {
        private readonly FileInfoCollection collection;

        public CollectionTypeStore(FileInfoCollection collection)
        {
            this.collection = collection;
        }
    }

    public class CollectionClassInfoStore : IClassInfoStore
    {
        private readonly FileInfoCollection collection;

        public CollectionClassInfoStore(FileInfoCollection collection)
        {
            this.collection = collection;
        }

        public ClassInfo this[string key]
        {
            get
            {
                foreach (var file in collection.Files)
                {
                    if (file.Value.ClassInfoStore.ClassInfo.ContainsKey(key))
                    {
                        return file.Value.ClassInfoStore.ClassInfo[key];
                    }
                }
                throw new KeyNotFoundException();
            }
        }

        public bool ContainsKey(string key)
        {
            foreach (var file in collection.Files)
            {
                if (file.Value.ClassInfoStore.ClassInfo.ContainsKey(key))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
