﻿using System.Collections.Generic;

namespace NicroWare.Analyzer.Master
{
    public class FileInfoCollection
    {
        public FileInfoCollection()
        {
            ClassInfoStore = new CollectionClassInfoStore(this);
            MethodStore = new CollectionMethodStore(this);
            TypeStore = new CollectionTypeStore(this);
        }
        public Dictionary<string, FileInformation> Files { get; } = new Dictionary<string, FileInformation>();
        public IClassInfoStore ClassInfoStore { get; } 

        public IMethodStore MethodStore { get; }

        public ITypeStore TypeStore { get; }
    }
}
