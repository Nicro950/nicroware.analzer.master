﻿namespace NicroWare.Analyzer.Master
{
    public class FileInformation
    {
        public TypeStore TypeStore { get; } = new TypeStore();
        public MethodStore MethodStore { get; } = new MethodStore();
        public ClassInfoStore ClassInfoStore { get; } = new ClassInfoStore();
    }
}
