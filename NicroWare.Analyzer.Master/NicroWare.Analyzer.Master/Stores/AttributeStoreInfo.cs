﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NicroWare.Analyzer.Master
{
    public class ArugmentListTypeInfo
    {
        public List<ArgumentTypeInfo> Arguments { get; private set; } = new List<ArgumentTypeInfo>();

        public ArgumentTypeInfo RegisterArgument()
        {
            ArgumentTypeInfo info = new ArgumentTypeInfo();
            Arguments.Add(info);
            return info;
        }
    }

    public class ArgumentTypeInfo
    {
        public List<RestrictionInfo> Restrictions { get; private set; } = new List<RestrictionInfo>();
        public bool Nullable { get; set; }
    }

    public class RestrictionInfo
    {
        public string Name { get; set; }
        public int LinkedTo { get; set; }
    }
}
