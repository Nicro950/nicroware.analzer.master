﻿using System.Collections.Generic;

namespace NicroWare.Analyzer.Master
{
    public class TypeStore
    {
        private readonly Dictionary<string, SubTypeInformation> subTypeInfo = new Dictionary<string, SubTypeInformation>();

        public void Add(string subType, string methodName)
        {
            if (!subTypeInfo.ContainsKey(subType))
            {
                subTypeInfo[subType] = new SubTypeInformation();
            }
            subTypeInfo[subType].CheckMethods.Add(methodName);
        }
    }

    public class SubTypeInformation
    {
        public List<string> CheckMethods { get; } = new List<string>();
    }
}
