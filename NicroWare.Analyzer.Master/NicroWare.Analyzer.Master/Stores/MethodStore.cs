﻿using System;
using System.Collections.Generic;

namespace NicroWare.Analyzer.Master
{
    public class MethodStore
    {
        private readonly Dictionary<string, CheckMethod> checkMethods = new Dictionary<string, CheckMethod>();
        private readonly HashSet<string> nullCheckMethods = new HashSet<string>();
        private readonly HashSet<string> returnNullMethods = new HashSet<string>();
        private readonly HashSet<string> passthroughMethods = new HashSet<string>();
        private readonly Dictionary<string, ArugmentListTypeInfo> methodInfos = new Dictionary<string, ArugmentListTypeInfo>();

        public void AddCheckMethod(string methodName, string subType, string[] parameters)
        {
            if (!checkMethods.ContainsKey(methodName))
                checkMethods.Add(methodName, new CheckMethod(methodName));
            checkMethods[methodName].Add(subType, parameters);
        }

        public bool ContainsCheckMethod(string methodName)
        {
            return checkMethods.ContainsKey(methodName);
        }

        public IReadOnlyCollection<SubTypeCheckMethod> GetChecksFor(string methodName)
        {
            return checkMethods[methodName].Restrictions.AsReadOnly();
        }

        public ArugmentListTypeInfo CreateAttributeListFor(string methodName)
        {
            ArugmentListTypeInfo info = new ArugmentListTypeInfo();
            methodInfos[methodName] = info;
            return info;
        }

        public bool ContainsMethodInfo(string methodName)
        {
            return methodInfos.ContainsKey(methodName);
        }

        public ArugmentListTypeInfo GetMethodInfo(string methodName)
        {
            return methodInfos[methodName];
        }

        public void AddNullCheck(string methodName)
        {
            nullCheckMethods.Add(methodName);
        }

        public void AddPassthrough(string methodName)
        {
            passthroughMethods.Add(methodName);
        }

        internal bool ContainsNullCheckMethod(string methodName)
        {
            return nullCheckMethods.Contains(methodName);
        }

        public void AddNullableMethod(string methodName)
        {
            returnNullMethods.Add(methodName);
        }

        internal bool ContainsNullableMethod(string methodName)
        {
            return returnNullMethods.Contains(methodName);
        }

        internal bool ContainsPassthroughMethod(string methodName)
        {
            return passthroughMethods.Contains(methodName);
        }
    }

    public class CheckMethod
    {
        public CheckMethod(string methodName)
        {
            this.Name = methodName;
        }

        public string Name { get; set; }
        public List<SubTypeCheckMethod> Restrictions { get; set; } = new List<SubTypeCheckMethod>();

        public void Add(string subType, IEnumerable<string> parameters)
        {
            Restrictions.Add(new SubTypeCheckMethod() { SubType = subType, Parameters = new List<string>(parameters) });
        }
    }

    public class SubTypeCheckMethod
    {
        public string SubType { get; set; }
        public List<string> Parameters { get; set; } = new List<string>();
    }
}
