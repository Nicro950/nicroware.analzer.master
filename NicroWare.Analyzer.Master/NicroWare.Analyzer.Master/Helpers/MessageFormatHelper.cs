﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace NicroWare.Analyzer.Master
{
    public class MessageFormatHelper
    {
        public static string MissingRestrictions(string variable, params string[] values) => MissingRestrictions(variable, (IEnumerable<string>)values);

        public static string MissingRestrictions(string variable, IEnumerable<string> values)
        {
            return $"Argument \"{variable}\" missing restrictions:{Environment.NewLine}{string.Join(Environment.NewLine, values.Select(x => "- " + x))}";
        }

        public static string ContainsRestrictions(string variable, params string[] values) => ContainsRestrictions(variable, (IEnumerable<string>)values);

        public static string ContainsRestrictions(string variable, IEnumerable<string> values)
        {
            return $"Argument \"{variable}\" has the following restrictions:{Environment.NewLine}{string.Join(Environment.NewLine, values.Select(x => "- " + x))}";
        }

        public static string UnsportedException(SyntaxNode expression)
        {
            return $"Does not support {expression.GetType().ToString()} in context checking";
        }
    }
}
