﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Text;

namespace NicroWare.Analyzer.Master.Context
{
    public class FirstPassContextInfo
    {
        public SyntaxNode SyntaxNode { get; set; }

        public FileInformation FileInformation { get; set; }
    }
}
