﻿using System.Collections.Generic;

namespace NicroWare.Analyzer.Master
{
    public class ClassInfo
    {
        public Dictionary<string, ClassFieldInfo> AllClassField { get; set; } = new Dictionary<string, ClassFieldInfo>();
        public string Name { get; internal set; }

        public void Add(ClassFieldInfo info)
        {
            AllClassField[info.Name] = info;
        }
    }
}
