﻿using System.Collections.Generic;

namespace NicroWare.Analyzer.Master
{
    public class ClassInfoStore
    {
        public Dictionary<string, ClassInfo> ClassInfo { get; set; } = new Dictionary<string, ClassInfo>();

        public void Add(ClassInfo ci)
        {
            ClassInfo[ci.Name] = ci;
        }
    }
}
