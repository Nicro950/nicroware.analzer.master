﻿using System;
using System.Collections.Generic;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace NicroWare.Analyzer.Master
{
    public class ContextInfo
    {
        public SyntaxNode SyntaxNode { get; set; }
        public ContextInfo ParentInfo { get; set; }
        public ContextScope ContextScope { get; set; } = new ContextScope();

        public int Level
        {
            get
            {
                if (ParentInfo == null)
                    return 0;
                else return ParentInfo.Level + 1;
            }
        }

        public ContextInfo ScopeParent
        {
            get
            {
                if (this.ParentInfo == null || this.SyntaxNode is BlockSyntax)
                {
                    return this;
                }
                else
                {
                    return ParentInfo.ScopeParent;
                }
            }
        }

        public override string ToString()
        {
            return $"ContextInfo at level: {Level}";
        }

        private ContextScope TotalScope
        {
            get
            {
                ContextScope scope = new ContextScope();
                this.GetTotalInfo(scope);
                return scope;
            }
        }

        private void GetTotalInfo(ContextScope scope)
        {
            if (ParentInfo != null)
            {
                ParentInfo.GetTotalInfo(scope);
            }

            scope.AddFromScope(this.ContextScope);
        }

        public VariableScope GetCurrentScope(string name)
        {
            VariableScope scope = new VariableScope();
            FillCurrentScope(name, scope);
            return scope;
        }

        private void FillCurrentScope(string name, VariableScope scope)
        {
            if (ParentInfo != null)
                ParentInfo.FillCurrentScope(name, scope);

            if (this.ContextScope.Variables.ContainsKey(name))
                scope.AddScope(this.ContextScope.Variables[name]);
        }

        public void AddRange(string name, IEnumerable<string> restrictions)
        {
            ContextScope.Add(name, restrictions);
        }

        public VariableScope GetCurrentScope(string[] path)
        {
            return GetCurrentScope(path, 0);
        }

        private VariableScope GetCurrentScope(string[] path, int index)
        {
            VariableScope scope = new VariableScope();
            if (index < path.Length)
            {
                FillCurrentScope(path[0], scope);
                scope.GetSubScope(path, 1);
            }
            return scope;
        }

        public ContextInfo WithNode(SyntaxNode node)
        {
            return new ContextInfo
            {
                ParentInfo = this,
                SyntaxNode = node,  
            };
        }
    }
}
