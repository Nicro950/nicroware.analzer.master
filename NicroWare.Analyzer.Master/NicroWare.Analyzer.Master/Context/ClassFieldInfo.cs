﻿using System.Collections.Generic;

namespace NicroWare.Analyzer.Master
{
    public class ClassFieldInfo
    {
        public string Name { get; set; }
        public List<Restriction> FixedRestrictions { get; set; } = new List<Restriction>();
        public bool IsNullable { get; set; }

        internal void AddRestriction(Restriction restriction)
        {
            FixedRestrictions.Add(restriction);
        }
    }
}
