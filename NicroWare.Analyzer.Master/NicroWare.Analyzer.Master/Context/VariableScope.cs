﻿using System.Collections.Generic;
using System.Linq;

namespace NicroWare.Analyzer.Master
{
    public class VariableScope
    {
        public VariableScope()
        {
        }

        public VariableScope(bool nullable)
            :this(nullable, nullable)
        {
        }

        public VariableScope(bool nullable, bool floatingNullable)
        {
            Nullable = nullable;
            FloatingNullable = floatingNullable;
        }

        public HashSet<Restriction> Restrictions { get; set; } = new HashSet<Restriction>();
        public HashSet<Restriction> FixedRestrictions { get; set; } = new HashSet<Restriction>();

        public ContextScope ObjectScope { get; set; } = new ContextScope();

        public bool Nullable { get; set; } = false;
        public bool FloatingNullable { get; set; } = false;

        public bool IsNullable => Nullable;

        public void AddScope(VariableScope variableScope)
        {
            foreach (var restriction in variableScope.Restrictions)
            {
                Restrictions.Add(restriction);
            }
            foreach (var restriction in variableScope.FixedRestrictions)
            {
                FixedRestrictions.Add(restriction);
            }
            Nullable = variableScope.Nullable;
            FloatingNullable = variableScope.FloatingNullable;
            ObjectScope.AddFromScope(variableScope.ObjectScope);
        }

        public void InvertThis()
        {
            VariableScope variableScope = Invert();
            this.Restrictions = variableScope.Restrictions;
            this.ObjectScope = variableScope.ObjectScope;
        }

        public bool Contains(string restriction)
        {
            Restriction r = Restriction.Parse(restriction);
            return Restrictions.Contains(r) || FixedRestrictions.Contains(r);
        }

        public VariableScope Copy()
        {
            return new VariableScope
            {
                Restrictions = new HashSet<Restriction>(this.Restrictions),
                FixedRestrictions = new HashSet<Restriction>(this.FixedRestrictions),
                ObjectScope = ObjectScope.Copy(),
                Nullable = Nullable,
                FloatingNullable = FloatingNullable,
            };
        }

        public VariableScope Invert()
        {
            return new VariableScope
            {
                Restrictions = new HashSet<Restriction>(Restrictions.Select(x => x.Invert())),
                ObjectScope = ObjectScope.Invert()
            };
        }

        public Restriction GetRestriction(string restriction)
        {
            Restriction r = Restriction.Parse(restriction);
            if (Restrictions.Contains(r))
                return r;
            if (FixedRestrictions.Contains(r))
                return r;
            return new Restriction();
        }

        public VariableScope GetSubScope(string[] path, int i)
        {
            if (i < path.Length)
            {
                if (ObjectScope.Variables.ContainsKey(path[i]))
                {
                    return ObjectScope.Variables[path[i]].GetSubScope(path, i + 1);
                }
            }
            return null;
        }
    }
}
