﻿using System;

namespace NicroWare.Analyzer.Master
{
    public struct Restriction
    {
        public Restriction(string name)
            : this(name, false, null)
        { }

        public Restriction(string name, bool inverted)
            : this(name, inverted, null)
        { }

        public Restriction(string name, string linkedTo)
            : this(name, false, linkedTo)
        { }

        public Restriction(string name, bool inverted, string linkedTo)
            : this()
        {
            Name = name;
            Inverted = inverted;
            LinkedTo = linkedTo;
        }

        public static Restriction Parse(string name)
        {
            bool inverted = false;
            string linkedTo = null;
            if (name.StartsWith("!"))
            {
                inverted = true;
                name = name.Substring(1);
            }
            int start = name.IndexOf('(');
            int end = name.IndexOf(')');
            if (start >= 0 || end >= 0)
            {
                if (start > 0 && end > 0 && end > start)
                {
                    linkedTo = name.Substring(start + 1, end - start - 1);
                    name = name.Substring(0, start);
                }
                else
                {
                    throw new FormatException("The restriction name: " + name + " has a bad format");
                }
            }
            return new Restriction(name, inverted, linkedTo);
        }

        public string Name;
        public bool Inverted;
        public string LinkedTo;

        public override string ToString()
        {
            return $"{(Inverted ? "!" : "")}{Name}{(LinkedTo == null ? "" : $"({LinkedTo})")}";
        }

        public Restriction Invert()
        {
            this.Inverted = !this.Inverted;
            return this;
        }
    }
}
