﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis;

namespace NicroWare.Analyzer.Master
{
    public class ContextScope
    {
        public Dictionary<string, VariableScope> Variables { get; set; } = new Dictionary<string, VariableScope>();

        public void Add(string key, VariableScope scope)
        {
            if (scope == null) throw new ArgumentNullException(nameof(scope));
            if (!Variables.ContainsKey(key))
            {
                Variables.Add(key, scope);
            }
            else
            {
                Variables[key].AddScope(scope);
            }
        }

        public void Add(string key, Restriction restriction, bool @fixed = false)
        {
            if (!Variables.ContainsKey(key))
            {
                if (@fixed)
                {
                    Variables.Add(key, new VariableScope { FixedRestrictions = new HashSet<Restriction>() { restriction } });
                }
                else
                {
                    Variables.Add(key, new VariableScope { Restrictions = new HashSet<Restriction>() { restriction } });
                }
            }
            else
            {
                if (@fixed)
                {
                    Variables[key].FixedRestrictions.Add(restriction);
                }
                else
                {
                    Variables[key].Restrictions.Add(restriction);
                }
            }
        }

        public void Add(string name, IEnumerable<string> restrictions)
        {
            Add(name, new VariableScope() { Restrictions = new HashSet<Restriction>(restrictions.Select(x => Restriction.Parse(x))) });
        }

        public void AddEmpty(string text)
        {
            if (!this.Variables.ContainsKey(text))
            {
                this.Variables[text] = new VariableScope();
            }
        }

        public void AddFromScope(ContextScope scope)
        {
            foreach (var pair in scope.Variables)
            {
                Add(pair.Key, pair.Value.Copy());
            }
        }

        public void AddRange(string key, IEnumerable<Restriction> restrictions)
        {
            foreach (Restriction restriction in restrictions)
            {
                this.Add(key, restriction);
            }
        }

        public ContextScope Copy() => Copy(x => x.Copy());

        public ContextScope Copy(Func<VariableScope, VariableScope> func)
        {
            Dictionary<string, VariableScope> copy = new Dictionary<string, VariableScope>();
            foreach (var pair in this.Variables)
            {
                copy[pair.Key] = func(pair.Value);
            }
            return new ContextScope { Variables = copy };
        }

        public ContextScope Invert() => Copy(x => x.Invert());

        public ContextScope Merge(ContextScope scope)
        {
            return ContextScope.Merge(this, scope);
        }

        public void Remove(string key)
        {
            this.Variables.Remove(key);
        }

        public void Remove(IEnumerable<string> path)
        {
            string s = path.FirstOrDefault();
            if (s == null) return;
            if (path.Count() == 1)
            {
                Remove(s);
            }
            else
            {
                if (Variables.ContainsKey(s))
                {
                    Variables[s].ObjectScope.Remove(path.Skip(1));
                }
            }
        }

        public static ContextScope Merge(ContextScope left, ContextScope right)
        {
            ContextScope newScope = new ContextScope();
            newScope.AddFromScope(left);
            newScope.AddFromScope(right);
            return newScope;
        }

        public ContextScope Intersection(ContextScope scope)
        {
            return Intersection(this, scope);
        }

        public static ContextScope Intersection(ContextScope left, ContextScope right)
        {
            // TODO: Implement Union of the two sets.
            return new ContextScope();
        }

        public bool MakeNullable(string identifier)
        {
            if (!Variables.ContainsKey(identifier))
            {
                Variables.Add(identifier, new VariableScope(true));
            }
            else
            {
                if (Variables[identifier].FloatingNullable)
                    Variables[identifier].Nullable = true;
                else
                    return false;
            }
            return true;
        }

        internal bool MakeNonNullable(string identifier)
        {
            if (!Variables.ContainsKey(identifier))
            {
                Variables.Add(identifier, new VariableScope(false, true));
            }
            else
            {
                Variables[identifier].Nullable = false;
            }
            return true;
        }

        public bool MakeNullable(IEnumerable<string> pathLeft)
        {
            string firstOrDefault = pathLeft.FirstOrDefault();
            if (firstOrDefault == null) return false;

            if (pathLeft.Count() == 1)
            {
                return MakeNullable(firstOrDefault);
            }
            else
            {
                if (Variables.ContainsKey(firstOrDefault))
                {
                    return Variables[firstOrDefault].ObjectScope.MakeNullable(pathLeft.Skip(1));
                }
                return true;
            }
        }

        public bool MakeNonNullable(IEnumerable<string> pathLeft)
        {
            string firstOrDefault = pathLeft.FirstOrDefault();
            if (firstOrDefault == null) return false;

            if (pathLeft.Count() == 1)
            {
               return MakeNonNullable(firstOrDefault);
            }
            else
            {
                if (Variables.ContainsKey(firstOrDefault))
                {
                    return Variables[firstOrDefault].ObjectScope.MakeNonNullable(pathLeft.Skip(1));
                }
                return true;
            }
        }
    }
}
