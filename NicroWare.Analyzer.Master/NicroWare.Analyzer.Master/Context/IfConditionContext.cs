﻿namespace NicroWare.Analyzer.Master
{
    public class IfConditionContext
    {
        public ContextScope IfCondition { get; set; } = new ContextScope();
        public ContextScope ElseCondition { get; set; } = new ContextScope();
    }
}
