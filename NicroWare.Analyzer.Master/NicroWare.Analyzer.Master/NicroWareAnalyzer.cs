using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;
using NicroWare.Analyzer.Master.Context;

namespace NicroWare.Analyzer.Master
{
    [DiagnosticAnalyzer(LanguageNames.CSharp)]
    public partial class NicroWareAnalyzer : DiagnosticAnalyzer
    {
        public const string DiagnosticId = "NicroWareAnalyzerMaster";
        private const string CheckMethodAttributeName = "NicroWare.Analyzer.ExtLib.CheckMethodAttribute";
        private const string SubTypeAttributeName = "NicroWare.Analyzer.ExtLib.SubTypeAttribute";
        private const string NullableAttributeName = "NicroWare.Analyzer.ExtLib.NullableAttribute";
        private const string CheckNotNullAttributeName = "NicroWare.Analyzer.ExtLib.CheckNotNullAttribute";

        private TypeRestrictions restrictions = new TypeRestrictions();

        // You can change these strings in the Resources.resx file. If you do not want your analyzer to be localize-able, you can use regular strings for Title and MessageFormat.
        // See https://github.com/dotnet/roslyn/blob/master/docs/analyzers/Localizing%20Analyzers.md for more on localization
        private static readonly LocalizableString Title = new LocalizableResourceString(nameof(Resources.AnalyzerTitle), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString MessageFormat = new LocalizableResourceString(nameof(Resources.AnalyzerMessageFormat), Resources.ResourceManager, typeof(Resources));
        private static readonly LocalizableString Description = new LocalizableResourceString(nameof(Resources.AnalyzerDescription), Resources.ResourceManager, typeof(Resources));
        private const string Category = "Syntax";
        private static DiagnosticDescriptor Rule = new DiagnosticDescriptor(DiagnosticId, Title, MessageFormat, Category, DiagnosticSeverity.Error, isEnabledByDefault: true, description: Description);
        private static DiagnosticDescriptor WarningRule = new DiagnosticDescriptor(DiagnosticId, Title, MessageFormat, Category, DiagnosticSeverity.Warning, isEnabledByDefault: true, description: Description);
        private static DiagnosticDescriptor DebugRule = new DiagnosticDescriptor(DiagnosticId, Title, MessageFormat, Category, DiagnosticSeverity.Info, isEnabledByDefault: true, description: Description);

        public override ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics { get { return ImmutableArray.Create(Rule, WarningRule, DebugRule); } }

        public override void Initialize(AnalysisContext context)
        {
            // TODO: Consider registering other actions that act on syntax instead of or in addition to symbols
            // See https://github.com/dotnet/roslyn/blob/master/docs/analyzers/Analyzer%20Actions%20Semantics.md for more information

            // context.RegisterSymbolAction(AnalyzeSymbol, SymbolKind.NamedType);
            // context.RegisterSyntaxNodeAction(MethodNodeCheck, SyntaxKind.MethodDeclaration);
            // context.RegisterSymbolAction(AnalyzeAttributesOnMethods, SymbolKind.Method);
            context.RegisterSyntaxNodeAction(AnalyzeCompilationUnit, SyntaxKind.CompilationUnit);
            context.RegisterCompilationAction(AnalyzeCompilation);
            fileInfoColleciton.Files["PreDef"] = new FileInformation();
            fileInfoColleciton.Files["PreDef"].MethodStore.AddPassthrough("System.Threading.Tasks.Task<TResult>.ConfigureAwait(bool)");
        }

        private void AnalyzeCompilation(CompilationAnalysisContext obj)
        {
            // throw new NotImplementedException();
        }

        int i = 0;

        private FileInfoCollection fileInfoColleciton = new FileInfoCollection();

        private void AnalyzeCompilationUnit(SyntaxNodeAnalysisContext context)
        {
            System.Diagnostics.Debug.WriteLine($"Running Analysis nr: {i} on file: {context.SemanticModel.SyntaxTree.FilePath}");
            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
            watch.Start();

            List<Compilation> allCompilations = new List<Compilation>();
            allCompilations.Add(context.Compilation);

            foreach (CompilationReference v in context.Compilation.ExternalReferences.OfType<CompilationReference>())
            {
                allCompilations.Add(v.Compilation);
            }


            SyntaxNode rootNode = context.Node;
            if (rootNode == null) return;
            List<(ContextInfo info, SyntaxNodeAnalysisContext context)> allInfos = new List<(ContextInfo info, SyntaxNodeAnalysisContext context)>();

            foreach (var com in allCompilations)
            {
                foreach (var tree in com.SyntaxTrees.Where(tree => tree == context.SemanticModel.SyntaxTree || !fileInfoColleciton.Files.ContainsKey(tree.FilePath)))
                {
                    var node = tree.GetRoot();
                    SyntaxNodeAnalysisContext context2 = new SyntaxNodeAnalysisContext(node, com.GetSemanticModel(tree), context.Options,
                        (Diagnostic d) =>
                        {
                            if (com == context.Compilation)
                            {
                                context.ReportDiagnostic(d);
                            }
                        },
                        (a) =>
                        {
                            return true;
                        }, context.CancellationToken);

                    ContextInfo info = new ContextInfo()
                    {
                        SyntaxNode = node,
                    };

                    FileInformation fileInfo = new FileInformation();
                    fileInfoColleciton.Files[tree.FilePath] = fileInfo;
                    ExploreChildNodesFirstPass(context2, node, fileInfo);
                    allInfos.Add((info, context2));
                }
            }
            string path = context.Compilation.SyntaxTrees.FirstOrDefault().FilePath;
            System.IO.DirectoryInfo di = new System.IO.FileInfo(path).Directory;
            while (di != null && di.GetFiles("*.csproj").Length == 0)
            {
                di = di.Parent;
            }
            if (di != null)
            {
                string nullable = di.GetFiles("Nullable.txt").FirstOrDefault()?.FullName;
                if (nullable != null)
                {
                    string[] lines = System.IO.File.ReadAllLines(nullable);
                    FileInformation fileInformation = new FileInformation();
                    fileInfoColleciton.Files["Nullable.txt"] = fileInformation;
                    foreach (string line in lines.Where(x => !string.IsNullOrWhiteSpace(x)))
                    {
                        fileInformation.MethodStore.AddNullableMethod(line);
                    }
                }
                string passthrough = di.GetFiles("Passthrough.txt").FirstOrDefault()?.FullName;

            }

            foreach ((ContextInfo info, SyntaxNodeAnalysisContext context) ci in allInfos)
            {
                ExploreChildNodes(ci.context, ci.info);
            }
            watch.Stop();
            System.Diagnostics.Debug.WriteLine("Analysis nr: " + i++ + " done in " + watch.ElapsedMilliseconds + " ms");
        }

        private void ExploreMethodDeclaration(SyntaxNodeAnalysisContext context, ContextInfo info, BaseMethodDeclarationSyntax method)
        {
            // TODO: Check if any returnable variables are null, and if the method can return null
            ContextInfo methodInfo = new ContextInfo
            {
                ParentInfo = info,
                SyntaxNode = method.Body
            };
            // System.Diagnostics.Debug.WriteLine("New block received!", "Names");

            var methodSymbol = context.SemanticModel.GetDeclaredSymbol(method);
            foreach (var parameter in methodSymbol.Parameters)
            {
                List<string> subTypes = new List<string>();

                var attributes = parameter.GetAttributes();
                if (attributes == null) continue;

                bool nullable = false;

                foreach (var attribute in attributes)
                {
                    if (attribute.AttributeClass.ToString() == SubTypeAttributeName)
                    {
                        string name = attribute.ConstructorArguments.FirstOrDefault().Value as string;
                        if (name != null)
                        {
                            subTypes.Add(name);
                        }
                    }
                    else if (attribute.AttributeClass.ToString() == NullableAttributeName)
                    {
                        nullable = true;
                    }
                }
                if (parameter.HasExplicitDefaultValue && parameter.ExplicitDefaultValue == null) nullable = true;
                methodInfo.AddRange(parameter.Name, subTypes);
                if (nullable) methodInfo.ContextScope.MakeNullable(parameter.Name);
                if (fileInfoColleciton.ClassInfoStore.ContainsKey(parameter.Type.ToDisplayString()))
                {
                    ClassInfo v = fileInfoColleciton.ClassInfoStore[parameter.Type.ToDisplayString()];
                    var varScope = methodInfo.ContextScope.Variables[parameter.Name];
                    foreach (KeyValuePair<string, ClassFieldInfo> val in v.AllClassField)
                    {
                        foreach (var restriction in val.Value.FixedRestrictions)
                        {
                            varScope.ObjectScope.Add(val.Key, restriction, true);
                        }
                        if (val.Value.IsNullable)
                        {
                            varScope.ObjectScope.MakeNullable(val.Key);
                        }
                    }

                }
            }

            if (methodInfo.SyntaxNode == null) return;

            ExploreChildNodes(context, methodInfo.SyntaxNode, methodInfo);
        }

        private HandleNodeState ExploreChildNodes(SyntaxNodeAnalysisContext context, ContextInfo info) => ExploreChildNodes(context, info.SyntaxNode.ChildNodes(), info);

        private HandleNodeState ExploreChildNodes(SyntaxNodeAnalysisContext context, SyntaxNode node, ContextInfo info) => ExploreChildNodes(context, node.ChildNodes(), info);

        private HandleNodeState ExploreChildNodes(SyntaxNodeAnalysisContext context, IEnumerable<SyntaxNode> nodes, ContextInfo parent)
        {
            bool returned = false;
            foreach (SyntaxNode n in nodes)
            {
                ContextInfo currentInfo = parent.WithNode(n);

                HandleNodeState state = HandleNode(context, currentInfo);
                returned = state.Returned || returned;
                // System.Diagnostics.Debug.WriteLine($"{new string(Enumerable.Repeat('\t', currentInfo.Level).ToArray())}parentNode: {parent.Level}, currentInfo: {currentInfo.Level}        " + n.GetType().FullName + " => " + n.ToString(), "Names");
            }
            return new HandleNodeState { Returned = returned };
        }

        private HandleNodeState HandleNode(SyntaxNodeAnalysisContext context, ContextInfo info)
        {
            HandleNodeState state = HandleNodeInner(context, info);
            if (state.ExploreChildren)
            {
                if (state.NextNodes != null)
                {
                    ExploreChildNodes(context, state.NextNodes, info);
                }
                else
                {
                    ExploreChildNodes(context, info);
                }
            }
            return state;
        }

        private HandleNodeState HandleNodeInner(SyntaxNodeAnalysisContext context, ContextInfo info)
        {
            switch (info.SyntaxNode)
            {
                case AssignmentExpressionSyntax aes:
                    HandleAssignmentExpression(context, aes, info);
                    return new HandleNodeState();
                case AwaitExpressionSyntax aes:
                    return HandleAwaitExpression(context, aes, info);
                case BlockSyntax bs:
                    return ExploreChildNodes(context, info);
                case ConditionalExpressionSyntax ces:
                    return HandleConditionalExpression(context, ces, info);
                case ConstructorDeclarationSyntax constds:
                    ExploreMethodDeclaration(context, info, constds);
                    return new HandleNodeState();
                case ClassDeclarationSyntax cds:
                    ExtractFieldInformation(context, info, cds);
                    ExtractPropInformation(context, info, cds);
                    break;
                case ElementAccessExpressionSyntax eaes:
                    AnalyzeElementAccess(context, info, eaes);
                    break;
                case ExpressionStatementSyntax ess:
                    return new HandleNodeState() { NextNodes = new SyntaxNode[] { ess.Expression }, ExploreChildren = true };
                case IfStatementSyntax iss:
                    AnalyzeIfStatement(context, iss, info);
                    return new HandleNodeState();
                case InvocationExpressionSyntax ies:
                    return HandleInvocation(context, ies, info);
                case LocalDeclarationStatementSyntax ldss:
                    HandleLocalDelcarationStatement(context, ldss, info);
                    return new HandleNodeState();
                case MethodDeclarationSyntax mds:
                    ExploreMethodDeclaration(context, info, mds);
                    return new HandleNodeState();
                case NamespaceDeclarationSyntax nds:
                    return new HandleNodeState() { NextNodes = nds.Members, ExploreChildren = true };
                case ObjectCreationExpressionSyntax oces:
                    return HandleObjectCreationScope(context, info, oces);
                case ParenthesizedExpressionSyntax pes:
                    return HandleNode(context, info.WithNode(pes.Expression));
                case PrefixUnaryExpressionSyntax preues:
                    HandleModificationByExpressionWithIdentifier(context, preues, preues.Operand, info);
                    return new HandleNodeState();
                case PostfixUnaryExpressionSyntax postUes:
                    HandleModificationByExpressionWithIdentifier(context, postUes, postUes.Operand, info);
                    return new HandleNodeState();
                case ReturnStatementSyntax returnStatement:
                    return HandleReturnStatement(context, returnStatement, info);
                case ThrowStatementSyntax throwStatement:
                    return new HandleNodeState { Returned = true, ExploreChildren = true };
                case VariableDeclarationSyntax vds:
                    AnalyzeVariableDeclaration(info, vds);
                    return new HandleNodeState();
                case UsingDirectiveSyntax uds:
                    return new HandleNodeState();

                case ConditionalAccessExpressionSyntax caes:
                    return HandleConditionalAccessExpression(context, caes, info);

                case AnonymousObjectCreationExpressionSyntax aoces:
                    return HandleAnonymousObjectCreation(context, aoces, info);

                //Normal explore childs, probably not reach any more since PropertyDeclarationSyntax is ignored
                case LiteralExpressionSyntax les:
                    return HandleLiteralExpression(context, les, info);
                case AccessorListSyntax accls:          // Should probalby have handle node on inner get and set
                case AccessorDeclarationSyntax ads:
                case SimpleLambdaExpressionSyntax sles:
                case ParenthesizedLambdaExpressionSyntax ples:
                    break;

                // Should be ignored
                case InterpolatedStringExpressionSyntax ises: // Should maybe be handled in the future.
                case AliasQualifiedNameSyntax aqns:     // Not sure how to incorporate
                case BaseListSyntax bls:
                case AttributeListSyntax als:
                case FieldDeclarationSyntax fds:        // Handled in ClassDeclarationSyntax
                case PropertyDeclarationSyntax pds:     // Handled in ClassDeclarationSyntax
                case InterfaceDeclarationSyntax ids:
                    return new HandleNodeState();

                case IdentifierNameSyntax ins:
                    return HandleIdentifierNameSyntax(context, ins, info);
                case MemberAccessExpressionSyntax maes: // Should this handle the null checking ?
                    return HandleMemberAccessExpression(context, maes, info);

                default:
                    LogUnsupported(info.SyntaxNode);
                    break;
            }
            return new HandleNodeState() { ExploreChildren = true };
        }

        private HandleNodeState HandleConditionalExpression(SyntaxNodeAnalysisContext context, ConditionalExpressionSyntax ces, ContextInfo info)
        {
            IfConditionContext scope = TraverseIfCondition(context, ces.Condition, info.WithNode(ces.Condition));
            ContextInfo ifBlock = new ContextInfo
            {
                ParentInfo = info,
                SyntaxNode = ces.WhenTrue,
                ContextScope = scope.IfCondition.Copy()
            };
            var ifState = HandleNode(context, ifBlock);

            ContextInfo elseBlock = new ContextInfo
            {
                ParentInfo = info,
                SyntaxNode = ces.WhenFalse,
                ContextScope = scope.ElseCondition.Copy()
            };
            var elseState = HandleNode(context, elseBlock);

            return new HandleNodeState()
            {
                ReturnScope = new VariableScope
                {
                    Nullable = (ifState.ReturnScope?.IsNullable ?? false) || (elseState.ReturnScope?.IsNullable ?? false),
                },
            };

        }

        private HandleNodeState HandleAwaitExpression(SyntaxNodeAnalysisContext context, AwaitExpressionSyntax aes, ContextInfo info)
        {
            var innerScope = HandleNode(context, info.WithNode(aes.Expression));
            if (innerScope.ReturnScope != null && innerScope.ReturnScope.ObjectScope.Variables.ContainsKey("$Task"))
            {
                return new HandleNodeState() { ReturnScope = innerScope.ReturnScope.ObjectScope.Variables["$Task"] };
            }
            return new HandleNodeState();
        }

        private HandleNodeState HandleConditionalAccessExpression(SyntaxNodeAnalysisContext context, ConditionalAccessExpressionSyntax caes, ContextInfo info)
        {
            var leftScope = HandleNode(context, info.WithNode(caes.Expression));

            ContextInfo newInfo = info.WithNode(caes.WhenNotNull);
            if (leftScope.ReturnScope != null)
            {
                leftScope.ReturnScope.Nullable = false;
                newInfo.ContextScope.Add("$", leftScope.ReturnScope);
            }

            var rightScope = HandleNode(context, newInfo);

            VariableScope returnScope = rightScope.ReturnScope ?? new VariableScope(true);
            returnScope.Nullable = true;

            return new HandleNodeState()
            {
                ReturnScope = returnScope,
            };
        }

        private HandleNodeState HandleReturnStatement(SyntaxNodeAnalysisContext context, ReturnStatementSyntax returnStatement, ContextInfo info)
        {
            if (returnStatement.Expression != null)
            {
                var scope = HandleNode(context, info.WithNode(returnStatement.Expression));
                if (scope.ReturnScope != null)
                {
                    SyntaxNode n = returnStatement.Parent;
                    while (n != null && !(n is MethodDeclarationSyntax))
                    {
                        n = n.Parent;
                    }
                    MethodDeclarationSyntax syntax = n as MethodDeclarationSyntax;
                    if (n != null)
                    {
                        IMethodSymbol methodSymbol = context.SemanticModel.GetDeclaredSymbol(syntax);
                        if (!fileInfoColleciton.MethodStore.ContainsNullableMethod(methodSymbol.ToString()) && scope.ReturnScope.IsNullable)
                        {
                            ReportNullableValue(context, returnStatement.Expression);
                        }
                    }
                }
            }
            return new HandleNodeState { Returned = true };
        }

        private HandleNodeState HandleLiteralExpression(SyntaxNodeAnalysisContext context, LiteralExpressionSyntax les, ContextInfo info)
        {
            switch (les.Kind())
            {
                case SyntaxKind.NullLiteralExpression:
                    return new HandleNodeState()
                    {
                        ReturnScope = new VariableScope()
                        {
                            Nullable = true,
                        },
                    };
                default:
                    return new HandleNodeState();
            }
        }

        private HandleNodeState HandleAnonymousObjectCreation(SyntaxNodeAnalysisContext context, AnonymousObjectCreationExpressionSyntax aoces, ContextInfo info)
        {
            VariableScope returnScope = new VariableScope();
            foreach (var field in aoces.Initializers)
            {
                var scope = HandleNode(context, info.WithNode(field.Expression));
                if (scope.ReturnScope != null)
                {
                    if (field.NameEquals != null)
                    {
                        returnScope.ObjectScope.Add(field.NameEquals.Name.Identifier.ValueText, scope.ReturnScope);
                    }
                    else if (field.Expression is IdentifierNameSyntax ins)
                    {
                        returnScope.ObjectScope.Add(ins.Identifier.ValueText, scope.ReturnScope);
                    }
                }
            }
            return new HandleNodeState()
            {
                ReturnScope = returnScope
            };
        }

        private HandleNodeState HandleIdentifierNameSyntax(SyntaxNodeAnalysisContext context, IdentifierNameSyntax ins, ContextInfo info)
        {
            return new HandleNodeState()
            {
                ReturnScope = info.GetCurrentScope(ins.Identifier.ValueText),
            };
        }

        private HandleNodeState HandleMemberAccessExpression(SyntaxNodeAnalysisContext context, MemberAccessExpressionSyntax maes, ContextInfo info)
        {
            var result = HandleNode(context, info.WithNode(maes.Expression));
            if (result.ReturnScope == null) return new HandleNodeState();

            if (result.ReturnScope.IsNullable)
            {
                ReportNullableValue(context, maes.Expression);
                return new HandleNodeState();
            }
            if (maes.Parent is InvocationExpressionSyntax) // Meaning that this is a invocation 
            {
                return new HandleNodeState()
                {
                    ReturnScope = result.ReturnScope,
                };
            }
            if (result.ReturnScope.ObjectScope.Variables.ContainsKey(maes.Name.Identifier.ValueText))
            {
                var someScope = result.ReturnScope.ObjectScope.Variables[maes.Name.Identifier.ValueText];
                return new HandleNodeState()
                {
                    ReturnScope = someScope,
                };
            }
            return new HandleNodeState();
        }

        private void HandleLocalDelcarationStatement(SyntaxNodeAnalysisContext context, LocalDeclarationStatementSyntax ldss, ContextInfo info)
        {
            foreach (var v in ldss.Declaration.Variables)
            {
                if (v.Initializer == null) continue;

                HandleNodeState state = HandleNode(context, info.WithNode(v.Initializer.Value));
                if (state.ReturnScope != null)
                {
                    info.ScopeParent.ContextScope.Add(v.Identifier.ValueText, state.ReturnScope);
                }
            }
        }

        private void HandleAssignmentExpression(SyntaxNodeAnalysisContext context, AssignmentExpressionSyntax aes, ContextInfo info)
        {
            VariableScope scope = HandleNode(context, info.WithNode(aes.Left)).ReturnScope;
            string[] pathLeft = GetPathForSyntax(aes.Left);
            if (scope == null) return;
            VariableScope scopeRight = HandleNode(context, info.WithNode(aes.Right)).ReturnScope;
            string nameRight = GetFriendlyName(aes.Right);


            List<string> missingFixedRestrictions = new List<string>();
            foreach (var fixedRestriction in scope.FixedRestrictions)
            {
                if (scopeRight == null || !(scopeRight.Restrictions.Contains(fixedRestriction) || scopeRight.FixedRestrictions.Contains(fixedRestriction)))
                {
                    missingFixedRestrictions.Add(fixedRestriction.Name);

                }
            }
            if (missingFixedRestrictions.Count > 0)
            {
                Diagnostic errorDiag = Diagnostic.Create(Rule, aes.Right.GetLocation(), MessageFormatHelper.MissingRestrictions(nameRight, missingFixedRestrictions));
                context.ReportDiagnostic(errorDiag);
            }
            RemoveRestrictions(pathLeft, info);

            if (scopeRight != null && scopeRight.Nullable)
            {
                if (!info.ScopeParent.ContextScope.MakeNullable(pathLeft))
                {
                    ReportNullableValue(context, aes.Right);
                }
            }
            else
            {
                if (!info.ScopeParent.ContextScope.MakeNonNullable(pathLeft))
                {
                    ReportNullableValue(context, aes.Right);
                }
                
            }
        }

        private void RemoveRestrictions(IEnumerable<string> pathLeft, ContextInfo info)
        {
            string firstOrDefault = pathLeft.FirstOrDefault();
            if (firstOrDefault == null) return;
            if (pathLeft.Count() == 1)
            {
                if (info.ScopeParent.ContextScope.Variables.ContainsKey(firstOrDefault))
                {
                    var removable = info.ScopeParent.ContextScope.Variables[firstOrDefault].Restrictions;

                    foreach (var v in removable.ToArray())
                    {
                        info.ScopeParent.ContextScope.Variables[firstOrDefault].Restrictions.Remove(v);
                    }
                }
            }
            else
            {
                RemoveRestrictions(pathLeft.Skip(1), info);
            }
        }

        private void ExtractVariableScope(ISymbol symbol, SyntaxToken identifier, ContextInfo info)
        {
            ImmutableArray<AttributeData> methodAttributes = symbol.GetAttributes();

            foreach (AttributeData attr in methodAttributes)
            {
                if (attr.AttributeClass.ToString() == SubTypeAttributeName)
                {
                    TypedConstant first = attr.ConstructorArguments.FirstOrDefault();
                    if (!(first.Value is string subType)) continue;

                    info.ContextScope.Add(identifier.ValueText, new Restriction(subType, false, null), true);
                }
                else if (attr.AttributeClass.ToString() == NullableAttributeName)
                {
                    info.ContextScope.MakeNullable(identifier.ValueText);
                }
            }
        }

        private void ExtractFieldInformation(SyntaxNodeAnalysisContext context, ContextInfo info, ClassDeclarationSyntax cds)
        {
            foreach (FieldDeclarationSyntax fields in cds.Members.OfType<FieldDeclarationSyntax>())
            {
                foreach (VariableDeclaratorSyntax variables in fields.Declaration.Variables)
                {
                    ISymbol symbol = context.SemanticModel.GetDeclaredSymbol(variables);
                    ExtractVariableScope(symbol, variables.Identifier, info);
                }
            }
        }

        private void ExtractPropInformation(SyntaxNodeAnalysisContext context, ContextInfo info, ClassDeclarationSyntax cds)
        {
            foreach (PropertyDeclarationSyntax property in cds.Members.OfType<PropertyDeclarationSyntax>())
            {
                ISymbol symbol = context.SemanticModel.GetDeclaredSymbol(property);
                ExtractVariableScope(symbol, property.Identifier, info);
            }
        }

        private void AnalyzeIfStatement(SyntaxNodeAnalysisContext context, IfStatementSyntax iss, ContextInfo info)
        {
            IfConditionContext scope = TraverseIfCondition(context, iss.Condition, info.WithNode(iss.Condition));
            ContextInfo ifBlock = new ContextInfo
            {
                ParentInfo = info,
                SyntaxNode = iss.Statement,
                ContextScope = scope.IfCondition.Copy()
            };
            HandleNodeState state = HandleNode(context, ifBlock);

            if (iss.Else != null)
            {
                ContextInfo elseBlock = new ContextInfo
                {
                    ParentInfo = info,
                    SyntaxNode = iss.Else.Statement,
                    ContextScope = scope.ElseCondition.Copy()
                };
                HandleNode(context, elseBlock);
            }

            if (state.Returned)
            {
                info.ScopeParent.ContextScope.AddFromScope(scope.ElseCondition);
            }
        }

        private IfConditionContext TraverseIfCondition(SyntaxNodeAnalysisContext context, ExpressionSyntax syntax, ContextInfo info)
        {
            switch (syntax)
            {
                case InvocationExpressionSyntax ies:
                    return CalculateInvocationScope(context, ies, info);
                case PrefixUnaryExpressionSyntax pes when pes.OperatorToken.ValueText == "!":
                    return InvertScope(TraverseIfCondition(context, pes.Operand, info));

                case BinaryExpressionSyntax bes when bes.OperatorToken.ValueText == "&&":
                    return HandleIfAndScopeMerge(context, bes, info);
                case BinaryExpressionSyntax bes when bes.OperatorToken.ValueText == "||":
                    return HandleIfOrScopeMerge(context, bes, info);

                case BinaryExpressionSyntax bes when bes.OperatorToken.ValueText == "==":
                    return HandleIfEquelNullCheck(context, bes, info);
                case BinaryExpressionSyntax bes when bes.OperatorToken.ValueText == "!=":
                    return InvertScope(HandleIfEquelNullCheck(context, bes, info));

                case ParenthesizedExpressionSyntax pess:
                    return TraverseIfCondition(context, pess.Expression, info);
                default:
                    HandleNode(context, info.WithNode(syntax));
                    break;
            }
            return new IfConditionContext();
        }

        private IfConditionContext InvertScope(IfConditionContext context)
            => new IfConditionContext { IfCondition = context.ElseCondition.Copy(), ElseCondition = context.IfCondition.Copy() };


        private IfConditionContext HandleIfEquelNullCheck(SyntaxNodeAnalysisContext context, BinaryExpressionSyntax bes, ContextInfo info)
        {
            IfConditionContext ifContext = new IfConditionContext();
            var leftScope = HandleNode(context, info.WithNode(bes.Left));
            var rightScope = HandleNode(context, info.WithNode(bes.Right));
            if (bes.Right is LiteralExpressionSyntax lesRight && lesRight.Kind() == SyntaxKind.NullLiteralExpression)
            {
                string[] path = GetPathForSyntax(bes.Left);
                if (path != null)
                {
                    ifContext.IfCondition.MakeNullable(path);
                    ifContext.ElseCondition.MakeNonNullable(path);
                }
            }
            else if (bes.Left is LiteralExpressionSyntax lesLeft && lesLeft.Kind() == SyntaxKind.NullLiteralExpression)
            {
                string[] path = GetPathForSyntax(bes.Right);
                if (path != null)
                {
                    ifContext.IfCondition.MakeNullable(path);
                    ifContext.ElseCondition.MakeNonNullable(path);
                }
            }
            return ifContext;
        }

        private IfConditionContext HandleIfOrScopeMerge(SyntaxNodeAnalysisContext context, BinaryExpressionSyntax bes, ContextInfo info)
        {
            IfConditionContext scope1 = TraverseIfCondition(context, bes.Left, info);
            info.ContextScope = scope1.IfCondition;
            IfConditionContext scope2 = TraverseIfCondition(context, bes.Right, info);

            ContextScope intersection = scope1.IfCondition.Intersection(scope2.IfCondition);
            ContextScope merged = scope1.ElseCondition.Merge(scope2.ElseCondition);

            

            return new IfConditionContext() { IfCondition = intersection, ElseCondition = merged };
        }

        private IfConditionContext HandleIfAndScopeMerge(SyntaxNodeAnalysisContext context, BinaryExpressionSyntax bes, ContextInfo info)
        {
            IfConditionContext scope1 = TraverseIfCondition(context, bes.Left, info);
            info.ContextScope = scope1.IfCondition;
            IfConditionContext scope2 = TraverseIfCondition(context, bes.Right, info);

            ContextScope merged = scope1.IfCondition.Merge(scope2.IfCondition);
            ContextScope intersection = scope1.ElseCondition.Intersection(scope2.ElseCondition);

            return new IfConditionContext() { IfCondition = merged, ElseCondition = intersection };
        }

        private IfConditionContext CalculateInvocationScope(SyntaxNodeAnalysisContext context, InvocationExpressionSyntax expression, ContextInfo info)
        {
            ReportIfCanBeNull(context, expression, info);
            IfConditionContext ifContext = new IfConditionContext();
            // ContextScope scope = new ContextScope();
            var methodSymbol = context.SemanticModel.GetSymbolInfo(expression).Symbol as IMethodSymbol;
            if (methodSymbol == null) return ifContext;

            if (fileInfoColleciton.MethodStore.ContainsCheckMethod(methodSymbol.ToString())
                && expression.ArgumentList.Arguments.Count > 0
                && expression.ArgumentList.Arguments.Count < 3)
            {
                string linkedName = null;

                if (expression.ArgumentList.Arguments.Count == 2)
                {
                    var secondPar = expression.ArgumentList.Arguments[1];
                    string[] path = GetPathForSyntax(secondPar.Expression);
                    if (path != null)
                    {
                        linkedName = string.Join(".", path);
                    }
                    else
                    {
                        ReportUnsupported(context, secondPar);
                    }
                }
                var firstArgument = expression.ArgumentList.Arguments.First();
                // TODO: Refactor this into one for IdentifierName and MemberAccess
                if (firstArgument.Expression is IdentifierNameSyntax ins)
                {
                    string txt = ins.Identifier.Text;

                    IReadOnlyCollection<SubTypeCheckMethod> checkMethodRestrictions = fileInfoColleciton.MethodStore.GetChecksFor(methodSymbol.ToString());

                    IEnumerable<Restriction> ifCondition = checkMethodRestrictions.Select(t => new Restriction(t.SubType, linkedName));
                    IEnumerable<Restriction> elseCondition = checkMethodRestrictions.Select(t => new Restriction(t.SubType, true, linkedName));

                    ifContext.IfCondition.AddRange(txt, ifCondition);
                    ifContext.ElseCondition.AddRange(txt, elseCondition);
                }
                else if (firstArgument.Expression is MemberAccessExpressionSyntax eaes)
                {
                    string[] path = GetPartsFromMemberAccess(eaes);
                    IReadOnlyCollection<SubTypeCheckMethod> checkMethodRestrictions = fileInfoColleciton.MethodStore.GetChecksFor(methodSymbol.ToString());

                    IEnumerable<Restriction> ifCondition = checkMethodRestrictions.Select(t => new Restriction(t.SubType, linkedName));
                    IEnumerable<Restriction> elseCondition = checkMethodRestrictions.Select(t => new Restriction(t.SubType, true, linkedName));

                    VariableScope ifScope = new VariableScope();
                    foreach (var v in ifCondition)
                    {
                        ifScope.Restrictions.Add(v);
                    }
                    VariableScope elseScope = new VariableScope();
                    foreach (var v in elseCondition)
                    {
                        elseScope.Restrictions.Add(v);
                    }
                    ContextScope curIfScope = new ContextScope();
                    curIfScope.Add(path.Last(), ifScope);

                    ContextScope curElseScope = new ContextScope();
                    curElseScope.Add(path.Last(), elseScope);
                    foreach (var v in path.Skip(1).Reverse().Skip(1))
                    {
                        ContextScope tempIfScope = new ContextScope();
                        tempIfScope.Add(v, new VariableScope { ObjectScope = curIfScope });
                        ContextScope tempElseScope = new ContextScope();
                        tempElseScope.Add(v, new VariableScope { ObjectScope = curElseScope });
                        curIfScope = tempIfScope;
                        curElseScope = tempElseScope;
                    }

                    ifContext.IfCondition.Add(path.First(), new VariableScope { ObjectScope = curIfScope });
                    ifContext.ElseCondition.Add(path.First(), new VariableScope { ObjectScope = curElseScope });
                }
                else
                {
                    var diagnostic = Diagnostic.Create(WarningRule, firstArgument.Expression.GetLocation(), $"{nameof(CalculateInvocationScope)} " + MessageFormatHelper.UnsportedException(firstArgument.Expression));
                    context.ReportDiagnostic(diagnostic);
                }
            }
            else if (fileInfoColleciton.MethodStore.ContainsNullCheckMethod(methodSymbol.ToString()) 
                && expression.ArgumentList.Arguments.Count == 1)
            {
                var firstArgument = expression.ArgumentList.Arguments.First();
                // TODO: Refactor this into one for IdentifierName and MemberAccess
                if (firstArgument.Expression is IdentifierNameSyntax ins)
                {
                    string identifier = ins.Identifier.Text;


                    ifContext.IfCondition.MakeNonNullable(identifier);
                    ifContext.ElseCondition.MakeNullable(identifier);
                }
                /*else if (firstArgument.Expression is MemberAccessExpressionSyntax eaes)
                {
                    string[] path = GetPartsFromMemberAccess(eaes);
                    
                    VariableScope ifScope = new VariableScope();
                    foreach (var v in ifCondition)
                    {
                        ifScope.Restrictions.Add(v);
                    }
                    VariableScope elseScope = new VariableScope();
                    foreach (var v in elseCondition)
                    {
                        elseScope.Restrictions.Add(v);
                    }
                    ContextScope curIfScope = new ContextScope();
                    curIfScope.Add(path.Last(), ifScope);

                    ContextScope curElseScope = new ContextScope();
                    curElseScope.Add(path.Last(), elseScope);
                    foreach (var v in path.Skip(1).Reverse().Skip(1))
                    {
                        ContextScope tempIfScope = new ContextScope();
                        tempIfScope.Add(v, new VariableScope { ObjectScope = curIfScope });
                        ContextScope tempElseScope = new ContextScope();
                        tempElseScope.Add(v, new VariableScope { ObjectScope = curElseScope });
                        curIfScope = tempIfScope;
                        curElseScope = tempElseScope;
                    }

                    ifContext.IfCondition.Add(path.First(), new VariableScope { ObjectScope = curIfScope });
                    ifContext.ElseCondition.Add(path.First(), new VariableScope { ObjectScope = curElseScope });
                }*/
                else
                {
                    var diagnostic = Diagnostic.Create(WarningRule, firstArgument.Expression.GetLocation(), $"{nameof(CalculateInvocationScope)} " + MessageFormatHelper.UnsportedException(firstArgument.Expression));
                    context.ReportDiagnostic(diagnostic);
                }
            }
            return ifContext;
        }

        private void AnalyzeVariableDeclaration(ContextInfo info, VariableDeclarationSyntax vds)
        {
            VariableDeclaratorSyntax name = vds.DescendantNodes().OfType<VariableDeclaratorSyntax>().FirstOrDefault();
            info.ContextScope.AddEmpty(name.Identifier.Text);
        }

        private void ReportNullableValue(SyntaxNodeAnalysisContext context, ExpressionSyntax expression)
        {
            string[] path = GetPathForSyntax(expression) ?? new string[] { "" };
            Diagnostic diag = Diagnostic.Create(Rule, expression.GetLocation(), MessageFormatHelper.MissingRestrictions(string.Join(".", path), "NonNullable"));
            context.ReportDiagnostic(diag);
        }

        // TODO: Add tests for Invocation return scope
        // TODO: Add support for returning subtypes, and have the correct types on return
        // TODO: If both subtype and nullable is present, then the return have to either be null or the subtype
        private HandleNodeState HandleInvocation(SyntaxNodeAnalysisContext context, InvocationExpressionSyntax invoc, ContextInfo info)
        {
            HandleNodeState state = new HandleNodeState();
            string name = invoc.Expression.ToString();

            if (name.Contains("Scope.Check")) DoScopeCheck(context, invoc, info);
            
            SymbolInfo symbol = context.SemanticModel.GetSymbolInfo(invoc);

            // if (ReportIfCanBeNull(context, invoc.Expression, info)) return state;

            var result = HandleNode(context, info.WithNode(invoc.Expression));
            if (result.ReturnScope != null && result.ReturnScope.Nullable)
            {
                ReportNullableValue(context, invoc.Expression);
                return state;
            }

            

            if (symbol.Symbol == null) return state;
            IMethodSymbol methodSymbol = symbol.Symbol as IMethodSymbol;

            IMethodSymbol usedSymbol = methodSymbol;
            if (usedSymbol.IsExtensionMethod && usedSymbol.ReducedFrom != null)
            {
                usedSymbol = usedSymbol.ReducedFrom;
            }
            else if (usedSymbol.IsGenericMethod)
            {
                usedSymbol = usedSymbol.ConstructedFrom;
            }

            if (invoc.HasTrailingTrivia)
            {
                var trail = invoc.GetTrailingTrivia();
                if (trail.ToString().Contains("GetName"))
                {
                    Diagnostic diag = Diagnostic.Create(DebugRule, invoc.GetLocation(), usedSymbol.ToString());
                    context.ReportDiagnostic(diag);
                }
            }

            if (fileInfoColleciton.MethodStore.ContainsMethodInfo(usedSymbol.ToString()))
            {
                AnalyzeKnownArgumentList(context, info, invoc.ArgumentList, usedSymbol);
            }
            else
            {
                AnalyzeUnknownArgumentList(context, info, invoc.ArgumentList, usedSymbol);
            }

            VariableScope tempScope = new VariableScope();

            var returnTypeName = methodSymbol.ReturnType.ToDisplayString();


            if (fileInfoColleciton.MethodStore.ContainsPassthroughMethod(usedSymbol.ToString()) || fileInfoColleciton.MethodStore.ContainsPassthroughMethod(usedSymbol.OriginalDefinition.ToString()))
            {
                tempScope = result.ReturnScope ?? new VariableScope();
            }
            

            if (fileInfoColleciton.ClassInfoStore.ContainsKey(returnTypeName))
            {
                FillScopeWithType(returnTypeName, tempScope);
            }

            if (fileInfoColleciton.MethodStore.ContainsNullableMethod(usedSymbol.ToString()))
            {
                tempScope.Nullable = true;
            }

            if (usedSymbol.ReturnType.OriginalDefinition.ToString() == "System.Threading.Tasks.Task<TResult>")
            {
                var wrappedScope = new VariableScope();
                wrappedScope.ObjectScope.Variables["$Task"] = tempScope;
                tempScope = wrappedScope;
            }

            return new HandleNodeState()
            {
                ReturnScope = tempScope,
            };
        }

        private void AnalyzeKnownArgumentList(SyntaxNodeAnalysisContext context, ContextInfo info, BaseArgumentListSyntax argumentList, IMethodSymbol methodSymbol)
        {
            ArugmentListTypeInfo checks = fileInfoColleciton.MethodStore.GetMethodInfo(methodSymbol.ConstructedFrom.ToString());

            
            ArgumentSyntax[] arguments = argumentList.Arguments.ToArray();
            if (checks.Arguments.Count != arguments.Length) return;


            var pairs = arguments.Zip(checks.Arguments, (a, b) => (arg: a, check: b)).ToArray();

            foreach (var (argument, curRestrictions) in pairs)
            {
                string[] path = GetPathForSyntax(argument.Expression) ?? new string[] { $"{nameof(HandleInvocation)} " + MessageFormatHelper.UnsportedException(argument.Expression) };
                VariableScope scope = HandleNode(context, info.WithNode(argument.Expression)).ReturnScope ?? new VariableScope();

                List<string> missingRestrictions = CheckMissingRestrictions(info, curRestrictions, scope, pairs);
                if (missingRestrictions.Count > 0)
                {
                    Diagnostic diag = Diagnostic.Create(Rule, argument.Expression.GetLocation(), MessageFormatHelper.MissingRestrictions(string.Join(".", path), missingRestrictions));
                    context.ReportDiagnostic(diag);
                }

                if (scope.Nullable && !curRestrictions.Nullable)
                {
                    Diagnostic diag = Diagnostic.Create(Rule, argument.Expression.GetLocation(), MessageFormatHelper.MissingRestrictions(string.Join(".", path), "NonNullable"));
                    context.ReportDiagnostic(diag);
                }
            }
        }

        private void AnalyzeUnknownArgumentList(SyntaxNodeAnalysisContext context, ContextInfo info, BaseArgumentListSyntax argumentList, IMethodSymbol methodSymbol)
        {

            (ArgumentSyntax argument, IParameterSymbol parameter)[] arguments = argumentList.Arguments.Zip(methodSymbol.Parameters, (argument, parameter) => (argument, parameter)).ToArray();

            foreach (var pair in arguments)
            {
                string[] path = GetPathForSyntax(pair.argument.Expression) ?? new string[] { $"{nameof(HandleInvocation)} " + MessageFormatHelper.UnsportedException(pair.argument.Expression) };
                VariableScope scope = HandleNode(context, info.WithNode(pair.argument.Expression)).ReturnScope;

                if (scope == null) continue;

                if (scope.Nullable && !(pair.parameter.HasExplicitDefaultValue && pair.parameter.ExplicitDefaultValue == null))
                {
                    Diagnostic diag = Diagnostic.Create(Rule, pair.argument.Expression.GetLocation(), MessageFormatHelper.MissingRestrictions(string.Join(".", path), "NonNullable"));
                    context.ReportDiagnostic(diag);
                }
            }
        }

        private void AnalyzeElementAccessArgumentList(SyntaxNodeAnalysisContext context, ContextInfo info, BaseArgumentListSyntax argumentList)
        {

            var arguments = argumentList.Arguments;

            foreach (var argument in arguments)
            {
                string[] path = GetPathForSyntax(argument.Expression) ?? new string[] { $"{nameof(HandleInvocation)} " + MessageFormatHelper.UnsportedException(argument.Expression) };
                VariableScope scope = HandleNode(context, info.WithNode(argument.Expression)).ReturnScope;

                if (scope == null) continue;

                if (scope.Nullable )
                {
                    Diagnostic diag = Diagnostic.Create(Rule, argument.Expression.GetLocation(), MessageFormatHelper.MissingRestrictions(string.Join(".", path), "NonNullable"));
                    context.ReportDiagnostic(diag);
                }
            }
        }

        private void AnalyzeElementAccess(SyntaxNodeAnalysisContext context, ContextInfo info, ElementAccessExpressionSyntax eaes)
        {
            string name = eaes.Expression.ToString();

            SymbolInfo symbol = context.SemanticModel.GetSymbolInfo(eaes);

            if (symbol.Symbol == null) return;

            ReportIfCanBeNull(context, eaes.Expression, info);

            AnalyzeElementAccessArgumentList(context, info, eaes.ArgumentList);
        }

        private void DoScopeCheck(SyntaxNodeAnalysisContext context, InvocationExpressionSyntax invoc, ContextInfo info)
        {
            ArgumentSyntax argument = invoc.ArgumentList.Arguments.FirstOrDefault();
            if (argument == null) return;

            string[] path = GetPathForSyntax(argument.Expression) ?? new string[] { $"{nameof(HandleInvocation)} " + MessageFormatHelper.UnsportedException(argument.Expression) };
            VariableScope scope = HandleNode(context, info.WithNode(argument.Expression)).ReturnScope;

            if (scope == null)
            {
                ReportUnsupported(context, argument);
                return;
            }

            var v = scope.Restrictions.Concat(scope.FixedRestrictions).Select(x => x.ToString());
            if (scope.Nullable)
            {
                v = v.Concat(new string[] { "Nullable" });
            }
            Diagnostic diag = Diagnostic.Create(DebugRule, argument.Expression.GetLocation(), MessageFormatHelper.ContainsRestrictions(string.Join(".", path), v));
            context.ReportDiagnostic(diag);
        }


        private HandleNodeState HandleObjectCreationScope(SyntaxNodeAnalysisContext context, ContextInfo info, ObjectCreationExpressionSyntax oces)
        {
            HandleNodeState state = new HandleNodeState();
            VariableScope tempScope = state.ReturnScope = new VariableScope();

            SymbolInfo symbol = context.SemanticModel.GetSymbolInfo(oces);

            if (symbol.Symbol == null) return state;
            IMethodSymbol methodSymbol = symbol.Symbol as IMethodSymbol;
            if (oces.ArgumentList != null)
            {
                if (fileInfoColleciton.MethodStore.ContainsMethodInfo(methodSymbol.ToString()))
                {
                    AnalyzeKnownArgumentList(context, info, oces.ArgumentList, methodSymbol);
                }
                else
                {
                    AnalyzeUnknownArgumentList(context, info, oces.ArgumentList, methodSymbol);
                }
            }

            var name = symbol.Symbol.ContainingType.ToDisplayString();
            if (!fileInfoColleciton.ClassInfoStore.ContainsKey(name))
            {
                return state;
            }
            FillScopeWithType(name, tempScope);
            if (oces.Initializer != null)
            {
                ContextInfo contextInfo = new ContextInfo
                {
                    ParentInfo = info,
                    SyntaxNode = oces.Initializer
                };
                contextInfo.ContextScope.AddFromScope(tempScope.ObjectScope);
                foreach (var aes in oces.Initializer.Expressions.OfType<AssignmentExpressionSyntax>())
                {
                    HandleAssignmentExpression(context, aes, contextInfo);
                }
            }
            return state;
        }

        private void FillScopeWithType(string name, VariableScope tempScope)
        {
            if (fileInfoColleciton.ClassInfoStore.ContainsKey(name))
            {
                foreach (KeyValuePair<string, ClassFieldInfo> cfi in fileInfoColleciton.ClassInfoStore[name].AllClassField)
                {
                    foreach (Restriction restriction in cfi.Value.FixedRestrictions)
                    {
                        tempScope.ObjectScope.Add(cfi.Key, restriction, true);
                    }
                    if (cfi.Value.IsNullable)
                    {
                        tempScope.ObjectScope.MakeNullable(cfi.Key);
                    }
                }
            }
        }

        private static string[] GetPartsFromMemberAccess(MemberAccessExpressionSyntax maes)
        {
            List<string> allValues = new List<string>();
            GetPartsFromMemberAccess(maes, allValues);
            allValues.Reverse();
            return allValues.ToArray();
        }

        private static void GetPartsFromMemberAccess(MemberAccessExpressionSyntax maes, List<string> curList)
        {
            string outerValue = maes.Name.Identifier.ValueText;
            curList.Add(outerValue);
            if (maes.Expression is IdentifierNameSyntax ins)
            {
                curList.Add(ins.Identifier.ValueText);
            }
            else if (maes.Expression is MemberAccessExpressionSyntax innerMeas)
            {
                GetPartsFromMemberAccess(innerMeas, curList);
            }
            else
            {
                LogUnsupported(maes.Expression);
            }
        }

        private static void LogUnsupported(SyntaxNode syntax, [CallerMemberName]string callerName = "Unknown", [CallerFilePath]string filePath = "", [CallerLineNumber]int lineNr = -1)
        {
            string message = $"{nameof(ReportUnsupported)} From: {callerName} - {MessageFormatHelper.UnsportedException(syntax)} {{{syntax.ToString()} - {syntax.GetLocation().GetLineSpan().ToString()}}}. in {filePath} at {lineNr}";
            System.Diagnostics.Debug.WriteLine(message);
        }

        private static void ReportUnsupported(SyntaxNodeAnalysisContext context, ExpressionSyntax syntax, [CallerMemberName]string callerName = "Unknown", [CallerFilePath]string filePath = "", [CallerLineNumber]int lineNr = -1)
        {
            string message = $"{nameof(ReportUnsupported)} From: {callerName} - {MessageFormatHelper.UnsportedException(syntax)} {{{syntax.ToString()} - {syntax.GetLocation().GetLineSpan().ToString()}}}. in {filePath} at {lineNr}";
            System.Diagnostics.Debug.WriteLine(message);
            // Temporary disabled since it break all the tests, have some kind of optional 
            // flag for enabling this maybe?
            // Diagnostic warningDiag = Diagnostic.Create(WarningRule, syntax.GetLocation(), message);
            // context.ReportDiagnostic(warningDiag);
        }

        private static void ReportUnsupported(SyntaxNodeAnalysisContext context, ArgumentSyntax argument, [CallerMemberName]string callerName = "Unknown", [CallerFilePath]string filePath = "", [CallerLineNumber]int lineNr = -1)
        {
            ReportUnsupported(context, argument.Expression, callerName, filePath, lineNr);
        }

        private static string[] GetPathForSyntax(ExpressionSyntax expression)
        {
            switch (expression)
            {
                case IdentifierNameSyntax ins:
                    return new string[] { ins.Identifier.ValueText };
                case MemberAccessExpressionSyntax maes:
                    return GetPartsFromMemberAccess(maes);
                case PostfixUnaryExpressionSyntax postues:
                    return GetPathForSyntax(postues.Operand);
                case PrefixUnaryExpressionSyntax preues:
                    return GetPathForSyntax(preues.Operand);
                default:
                    return null;
            }
        }

        private static string GetFriendlyName(ExpressionSyntax expression)
        {
            switch (expression)
            {
                case IdentifierNameSyntax ins:
                case MemberAccessExpressionSyntax maes:
                    return string.Join(".", GetPathForSyntax(expression));
                default:
                    return "";
            }
        }



        private static List<string> CheckMissingRestrictions(ContextInfo info, ArgumentTypeInfo curRestrictions, VariableScope scope, (ArgumentSyntax, ArgumentTypeInfo)[] allParams) 
            => CheckMissingRestrictions(info, curRestrictions.Restrictions, scope, allParams);

        private static List<string> CheckMissingRestrictions(ContextInfo info, List<RestrictionInfo> curRestrictions, VariableScope scope, (ArgumentSyntax, ArgumentTypeInfo)[] allParams)
        {
            List<string> missingRestrictions = new List<string>();

            foreach (RestrictionInfo restriction in curRestrictions)
            {
                if (restriction.LinkedTo >= 0)
                {
                    if (allParams.Length > restriction.LinkedTo)
                    {
                        string[] testPath = GetPathForSyntax(allParams[restriction.LinkedTo].Item1.Expression);
                        if (testPath == null || !scope.Contains($"{restriction.Name}({string.Join(".", testPath)})"))
                        {
                            missingRestrictions.Add($"{restriction.Name}({string.Join(".", testPath)})");
                        }
                    }
                    else
                    {
                        missingRestrictions.Add(restriction.Name);
                    }
                }
                else if (!scope.Contains(restriction.Name))
                {
                    missingRestrictions.Add(restriction.Name);
                }
            }
            return missingRestrictions;
        }

        private void HandleModificationByExpressionWithIdentifier(SyntaxNodeAnalysisContext context, ExpressionSyntax aes, ExpressionSyntax identifier, ContextInfo info)
        {
            ReportIfCanBeNull(context, identifier, info);
            string[] path = GetPathForSyntax(identifier);
            if (path != null)
            {
                info.ScopeParent.ContextScope.Remove(path);
            }
        }

        private bool ReportIfCanBeNull(SyntaxNodeAnalysisContext context, ExpressionSyntax syntax, ContextInfo info)
        {
            var result = HandleNode(context, info.WithNode(syntax));
            if (result.ReturnScope != null && result.ReturnScope.Nullable)
            {
                ReportNullableValue(context, syntax);
                return true;
            }
            return false;
        }
    }

    public partial class NicroWareAnalyzer
    {
        // First pass Methods
        private HandleNodeState HandleNodeFirstPass(SyntaxNodeAnalysisContext context, FirstPassContextInfo info)
        {
            switch (info.SyntaxNode)
            {
                case NamespaceDeclarationSyntax nds:
                    return new HandleNodeState { ExploreChildren = true, NextNodes = nds.Members };
                case ClassDeclarationSyntax cds:
                    HandleTypeDeclarationFirstPass(context, info, cds);
                    return new HandleNodeState { ExploreChildren = true, NextNodes = cds.Members };
                case ConstructorDeclarationSyntax cds:
                    ExtractMethodInfoFirstPass(context, info, cds);
                    break;
                case MethodDeclarationSyntax mds:
                    ExtractMethodInfoFirstPass(context, info, mds);
                    break;
                case PropertyDeclarationSyntax pds:
                    break;
                case FieldDeclarationSyntax fds:
                    break;
                case UsingDirectiveSyntax uds:
                    break;
                case IncompleteMemberSyntax ims:
                    break;
                case InterfaceDeclarationSyntax ids:
                    HandleTypeDeclarationFirstPass(context, info, ids);
                    return new HandleNodeState { ExploreChildren = true, NextNodes = ids.Members };
                case AttributeListSyntax als:
                    break;
                case EnumDeclarationSyntax eds:
                    break;
                case StructDeclarationSyntax sds:
                    HandleTypeDeclarationFirstPass(context, info, sds);
                    return new HandleNodeState { ExploreChildren = true, NextNodes = sds.Members };
                default:
                    throw new NotSupportedException(info.SyntaxNode.GetType().ToString());
            }
            return new HandleNodeState { ExploreChildren = false };
        }


        private void ExploreChildNodesFirstPass(SyntaxNodeAnalysisContext context, SyntaxNode node, FileInformation fileInfo) => ExploreChildNodesFirstPass(context, node.ChildNodes(), fileInfo);

        private void ExploreChildNodesFirstPass(SyntaxNodeAnalysisContext context, IEnumerable<SyntaxNode> nodes, FileInformation fileInfo)
        {
            foreach (SyntaxNode sn in nodes)
            {
                var state = HandleNodeFirstPass(context, new FirstPassContextInfo { SyntaxNode = sn, FileInformation = fileInfo });
                if (state.ExploreChildren)
                {
                    if (state.NextNodes != null)
                        ExploreChildNodesFirstPass(context, state.NextNodes, fileInfo);
                    else
                        ExploreChildNodesFirstPass(context, sn, fileInfo);
                }
            }
        }


        private void HandleTypeDeclarationFirstPass(SyntaxNodeAnalysisContext context, FirstPassContextInfo info, TypeDeclarationSyntax cds)
        {
            var classSymbol = context.SemanticModel.GetDeclaredSymbol(cds);
            ClassInfo ci = new ClassInfo
            {
                Name = classSymbol.ToDisplayString(),
            };
            info.FileInformation.ClassInfoStore.Add(ci);

            var fields = cds.ChildNodes().OfType<FieldDeclarationSyntax>();
            foreach (FieldDeclarationSyntax field in fields)
            {
                ExtractFieldInformationFirstPass(context, field, ci);
            }

            var properties = cds.ChildNodes().OfType<PropertyDeclarationSyntax>();
            foreach (PropertyDeclarationSyntax prop in properties)
            {
                ExtractPropertyInformationFirstPass(context, prop, ci);
            }
        }


        private void ExtractFieldInformationFirstPass(SyntaxNodeAnalysisContext context, FieldDeclarationSyntax fds, ClassInfo classInfo)
        {
            foreach (VariableDeclaratorSyntax variable in fds.Declaration.Variables)
            {
                ISymbol variableSymbol = context.SemanticModel.GetDeclaredSymbol(variable);
                AddClassFieldInfo(variableSymbol, classInfo);
            }
        }

        private void ExtractPropertyInformationFirstPass(SyntaxNodeAnalysisContext context, PropertyDeclarationSyntax fds, ClassInfo classInfo)
        {
            ISymbol variableSymbol = context.SemanticModel.GetDeclaredSymbol(fds);
            AddClassFieldInfo(variableSymbol, classInfo);
        }

        private void AddClassFieldInfo(ISymbol variableSymbol, ClassInfo classInfo)
        {
            ClassFieldInfo info = new ClassFieldInfo
            {
                Name = variableSymbol.Name
            };
            classInfo.Add(info);

            ImmutableArray<AttributeData> methodAttributes = variableSymbol.GetAttributes();

            foreach (var attr in methodAttributes)
            {
                if (attr.AttributeClass.ToString() == SubTypeAttributeName)
                {
                    TypedConstant first = attr.ConstructorArguments.FirstOrDefault();
                    if (!(first.Value is string subType)) continue;

                    info.AddRestriction(new Restriction(subType));
                }
                else if (attr.AttributeClass.ToString() == NullableAttributeName)
                {
                    info.IsNullable = true;
                }
            }
        }


        private void ExtractMethodInfoFirstPass(SyntaxNodeAnalysisContext context, FirstPassContextInfo info, BaseMethodDeclarationSyntax mds)
        {
            var method = context.SemanticModel.GetDeclaredSymbol(mds);
            if (method == null) return;
            string methodName = method.ToString();

            var methodAttributes = method.GetAttributes();

            foreach (var attr in methodAttributes)
            {
                if (attr.AttributeClass.ToString() == CheckMethodAttributeName)
                {
                    var first = attr.ConstructorArguments.FirstOrDefault();
                    string subType = first.Value as string;
                    if (subType == null) continue;

                    string[] parameters = method.Parameters.Select(x => x.Name).Skip(1).ToArray();

                    info.FileInformation.MethodStore.AddCheckMethod(methodName, subType, parameters);

                    info.FileInformation.TypeStore.Add(subType, methodName);

                }
                else if (attr.AttributeClass.ToString() == CheckNotNullAttributeName)
                {
                    info.FileInformation.MethodStore.AddNullCheck(methodName);
                }
                else if (attr.AttributeClass.ToString() == NullableAttributeName)
                {
                    info.FileInformation.MethodStore.AddNullableMethod(methodName);
                }
            }

            var attrList = info.FileInformation.MethodStore.CreateAttributeListFor(method.ToString());

            List<(List<string> Restrictions, bool Nullable)> allParameters = new List<(List<string>, bool)>();

            foreach (var parameter in method.Parameters)
            {
                var attributes = parameter?.GetAttributes();
                if (attributes == null || !attributes.HasValue) continue;

                allParameters.Add(
                    (
                        attributes.Value
                            .Where(x => x.AttributeClass.ToString() == SubTypeAttributeName)
                            .Select(x => x.ConstructorArguments.FirstOrDefault().Value as string)
                            .Where(x => x != null).ToList(), 
                        attributes.Value
                            .Where(x => x.AttributeClass.ToString() == NullableAttributeName)
                            .Count() > 0 
                            || (parameter.HasExplicitDefaultValue && parameter.ExplicitDefaultValue == null)
                            || (parameter.Type.OriginalDefinition.SpecialType == SpecialType.System_Nullable_T)
                    ));
            }
            foreach (var parameter in allParameters)
            {
                ArgumentTypeInfo parameterRequirements = attrList.RegisterArgument();
                if (parameter.Nullable)
                {
                    parameterRequirements.Nullable = true;
                }
                foreach (var restriction in parameter.Restrictions)
                {
                    if (restriction.Contains("(this)")) continue;
                    var thisIndexes = allParameters
                        .Select((x, i) => x.Restrictions.IndexOf(restriction + "(this)") >= 0 ? i : -1)
                        .Where(x => x >= 0).ToArray();
                    int index = -1;
                    if (thisIndexes.Length == 1)
                        index = thisIndexes[0];
                    else if (thisIndexes.Length > 1)
                    {
                        var diag = Diagnostic.Create(DebugRule, mds.ParameterList.GetLocation(), $"Found multiple instances of {restriction}(this)");
                        context.ReportDiagnostic(diag);
                    }
                    parameterRequirements.Restrictions.Add(new RestrictionInfo() { Name = restriction, LinkedTo = index });
                }
            }
        }

    }


    public class HandleNodeState
    {
        public bool Returned { get; set; }
        public bool ExploreChildren { get; set; }
        public IEnumerable<SyntaxNode> NextNodes { get; set; }
        public VariableScope ReturnScope { get; set; }
    }

    public class AttributeInfo
    {
        private readonly AttributeSyntax syntax;
        private readonly SyntaxNodeAnalysisContext context;

        public string Name { get => syntax.Name.ToString(); }
        public string FullName
        {
            get
            {
                IMethodSymbol symbol = context.SemanticModel.GetSymbolInfo(syntax).Symbol as IMethodSymbol;
                if (symbol != null)
                    return symbol.ContainingType.ToString();
                return Name;
            }
        }

        public IReadOnlyCollection<ArgumentInfo> Arguments
        {
            get
            {
                return GetArguments(context, syntax.ArgumentList).Select(x => new ArgumentInfo() { Value = x }).ToList().AsReadOnly();
            }
        }

        public AttributeInfo(AttributeSyntax syntax, SyntaxNodeAnalysisContext context)
        {
            this.syntax = syntax;
            this.context = context;
        }

        public static List<string> GetArguments(SyntaxNodeAnalysisContext context, AttributeArgumentListSyntax list)
        {
            return list.Arguments
                .Select(x => x.Expression as LiteralExpressionSyntax)
                .Select(x =>
                {
                    if (x == null)
                        return null;
                    else
                        return x.Token.ValueText;
                }).ToList();
        }
    }

    public class ArgumentInfo
    {
        public string Value { get; set; }
    }

    public class TypeContext
    {
        public Dictionary<string, TypeRestrictions> AllRestrictions { get; } = new Dictionary<string, TypeRestrictions>();
    }

    public class TypeRestrictions
    {
        public string Name { get; set; }
        public string FullName { get; set; }
        public List<string> Restrictions { get; } = new List<string>();
    }
}
