﻿using System;

namespace NicroWare.Analyzer.Master.Helpers
{
    public class Walker<T>
    {
        private readonly T _obj;

        public Walker(T obj)
        {
            _obj = obj;
        }

        public T Obj => _obj;

        public bool HasValue { get => !_obj.Equals(default(T)); }


        public Walker<T2> Walk<T2>(Func<T, T2> selector)
        {
            if (_obj.Equals(default(T)))
                return new Walker<T2>(default(T2));
            T2 value = selector(_obj);
            return new Walker<T2>(value);
        }

        public Walker<T2> Walk<T2>(Func<T, T2> selector, Func<T2, bool> predicate)
        {
            if (_obj.Equals(default(T)))
                return new Walker<T2>(default(T2));
            T2 value = selector(_obj);

            if (value.Equals(default(T2)))
                return new Walker<T2>(default(T2));
            if (!predicate(value))
                return new Walker<T2>(default(T2));

            return new Walker<T2>(value);
        }

        public Walker<T> If(Func<T, bool> predicate)
        {
            if (_obj.Equals(default(T)))
                return new Walker<T>(default(T));
            if (!predicate(Obj))
                return new Walker<T>(default(T));

            return new Walker<T>(Obj);
        }
    }
}
