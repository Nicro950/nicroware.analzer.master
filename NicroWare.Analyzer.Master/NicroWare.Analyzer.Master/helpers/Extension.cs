﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Diagnostics;

namespace NicroWare.Analyzer.Master.Helpers
{
    public static class Extension
    {
        public static IEnumerable<TResult> SelectNotNull<T1, TResult>(this IEnumerable<T1> input, Func<T1, TResult> selector)
        {
            return input.Select(selector).Where(x => x != null);
        }

        public static Walker<T> CreateWalker<T>(this T value)
        {
            return new Walker<T>(value);
        }

        public static Walker<T2> Walk<T, T2>(this T value, Func<T, T2> selector)
        {
            Walker<T> w = new Walker<T>(value);
            return w.Walk(selector);
        }

        public static Walker<T2> Walk<T, T2>(this T value, Func<T, T2> selector, Func<T2, bool> predicate)
        {
            Walker<T> w = new Walker<T>(value);
            return w.Walk(selector, predicate);
        }

        public static List<AttributeInfo> GetAttributes(FieldDeclarationSyntax field, SyntaxNodeAnalysisContext context)
        {
            return field.DescendantNodes().OfType<AttributeSyntax>().Select(x => new AttributeInfo(x, context)).ToList();
        }

        public static IEnumerable<string> RetriveInformation(SyntaxNodeAnalysisContext obj)
        {
            var subTypeName = GetAttributes(obj.Node as FieldDeclarationSyntax, obj)
                .Where(x => x.FullName.StartsWith("NicroWare.Analyzer.ExtLib.SubTypeAttribute"))
                .Where(x => x.Arguments.Count > 0)
                .Select(x => x.Arguments.ElementAt(0).Value);

            return subTypeName;
        }

        public static string InvertRestriction(string s)
        {
            if (s.StartsWith("!"))
                return s.Substring(1);
            else return "!" + s;
        }
    }
}
